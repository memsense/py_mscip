# py_mscip

py_mscip is a python library for interfacing with Memsense IMU devices. This library implements the Memsense Communication Interface Protocol. Full documentation of this protocol can be found [here](https://www.memsense.com/assets/docs/uploads/ms-cip-eval/MS-CIP-Specification.pdf). A GUI based implementation can also be found [here](https://www.memsense.com/assets/docs/uploads/ms-cip-eval/MSCIP_Eval_Setup_R_1_1_12.msi).

## Installation

py_mscip is available as a PyPi package for convenience. To add the latest stable version to your project, install the library using pip:

`pip install py_mscip`

## Basic Usage

py_mscip provides a number of classes to make interfacing with Memsense IMUs convenient. The most basic of these is the Conn class which manages reading and writing to a serial connection. An example of a simple script to read values from the IMU and print them to the screen would be:

```python
from py_mscip.interfaces.conn import Conn
from serial import Serial

ser = Serial(args.comport, args.baudrate)
conn = Conn(ser, args.format)

while True:
    packets = conn.read()
    if packets:
        for packet in packets:
            print(packet)
```

Additional examples along with API details and development information can be found in the [official documentation](https://memsense.gitlab.io/py_mscip).