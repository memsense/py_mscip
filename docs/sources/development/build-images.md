# Creating Build Images

There are currently two docker images that are used for the CI pipeline. The first of these is a debian based linux image with python and the project requirements installed. This is done in order to reduce the run time of each stage in the CI process. The second image has the same dependencies installed but uses a windows base image. This image is needed as the CI does not offer much support for windows images. In fact, extra effort is taken in order to tell the CI to run in a windows VM where it can execute the windows container.

These images will only need to be rebuilt if additional requirements are added to the project.

## Building the Images

Both of the images are built locally and pushed to Gitlab's container repository where they are accessed by the CI. The container's build information can be found in `linux.Dockerfile` and `windows.Dockerfile`. When building the image it must be tagged using the container repository's naming convention otherwise the push will not work. The image name appends the python version that is pre installed with the base image.

`docker build -t registry.gitlab.com/memsense/py_mscip:linux-3.8.2 -f ./linux.Dockerfile .`

In order to push an image to the container repository you must be logged in through the docker program.

`docker login registry.gitlab.com`

This will present you with requires for your account information in order to sign in. You will now be able to push the local image to the Gitlab container repository.

`docker push registry.gitlab.com/memsense/py_mscip:linux-3.8.2`

The same process is done for creating the windows containers except replacing 'linux' in any of the commands with 'windows'.

## Running the Images

It's useful to be able to run the containers locally to verify changes were successful without having to wait on the CI to process the push. This command is slightly different when done on the linux container vs windows.

windows:
`docker run -v ${PWD}:c:\py_mscip registry.gitlab.com/memsense/py_mscip:windows-3.8.2 powershell "python -m pip install -e .; pytest"`

linux:
`docker run -v ${PWD}:/py_mscip registry.gitlab.com/memsense/py_mscip:linux-3.8.2 bash "python -m pip install -e . && pytest"`

Both of these commands will copy the current code directory into the docker image as a volume, install the current directory as a pip module, and run pytest.