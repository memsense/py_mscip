# Updating Documentation

Documentation uses `pydocmd-markdown` to automatically generate markdown files for the API documentation. This is a Python package that wraps `mkdocs` in order to automatically generate API documentation based on the docstrings found in the code.

## Local Development

A local development server is included with `pydocmd-markdown` to make it convenient to work on the documentation locally. The local server can be started by opening a terminal in the docs folder and running the command:

`pydocmd serve`

This will automatically rebuild the documentation when a change is detected. The documentation can be viewed by opening a browser to `localhost:8000`.

## Building Documentation

To build the documentation without a local server for production, use the following command:

`pydocmd build`