# Local Development

Developing the py_mscip project requires an alternative setup as apposed to using the PyPi package.

## Using Python's venv

Using python's virtual environment is recommended for keeping your system clean. A venv folder can be created in the root of the py_mscip project.

`python -m venv .venv`

The new virtual environment can now be activated using the activate script within the `.venv` folder. This script is in a slightly different location depending on the operating system being used. This script will set up your system's environment variables to reference the python binaries installed in the `.venv` folder.

Windows:
`.venv/Scripts/Activate.ps1`

Linux:
`source .venv/bin/activate`

When the python virtual environment is first created, you will also need to install dependencies to run the examples. 

`python -m pip install -r requirements.txt`

## Installing the Library

The project will also need to be installed itself: 

`pip install -e .`

## Running the code

Any of the examples or tests can now be ran and will correctly import the project's packages.

`python ./examples/conn_example.py <comport> <baudrate>`
or
`python ./examples/conn_example.py COM3 460800`
