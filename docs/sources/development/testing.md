# Testing

py_mscip uses `pytest` as its testing framework. A mock serial class has been created in order to test packages without needing an actual serial connection. In the future, it would be nice to add additional tests using an actual serial connection for local testing but this has not yet be created.

## Running Tests

Run all unit tests using the command:

`pytest`

A specific test can be ran by specifying the filename:

`pytest tests/test_Config.py`

Both methods will pretty print the results of the testing process. Any output statements will only be shown if the test fails for debugging purposes.