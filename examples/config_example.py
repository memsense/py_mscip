"""
This example shows how the Config class can be used to read a device's settings 
and save them to a json file. A second device is then created by reading the 
settings from the file and loading them to the device. 
"""

import argparse
import sys
from serial import Serial
from py_mscip.interfaces.conn import Conn
from py_mscip.interfaces.imu import IMU
from py_mscip.interfaces.config import Config


def get_arguments():
    """ 
    Get arguments from the command line 
    """
    parser = argparse.ArgumentParser(description="<ctl-c> to exit.")
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    """ 
    Read and write example for saving/loading IMU settings 
    """
    args = get_arguments()
    try:
        # Create a serial connection for the IMU to use
        ser = Serial(args.comport, args.baudrate)
        imu = IMU(ser)

        # Read the config from the device and save it to a file
        conf1 = Config()
        conf1.read(imu)
        conf1.save("config.json")

        # Load the config from a file and write it to the device
        conf2 = Config()
        conf2.load("config.json")
        conf2.write(imu)
        print(conf2)
    except:
        e = sys.exc_info()[0]
        print(e)
