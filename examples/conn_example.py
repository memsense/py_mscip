"""
This is a very simple example which shows how to use the Conn class to setup a
serial connection, read IMU values, and print them to the terminal. 
"""

import argparse
import math
from py_mscip.interfaces.conn import Conn
import py_mscip.packets.cip as cip
from serial import Serial


def get_arguments():
    """
    Get arguments from the command line 
    """
    parser = argparse.ArgumentParser(description="<ctl-c> to exit.")
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    parser.add_argument(
        "-f", "--format", nargs="+", choices=["hex", "info"], default="info"
    )
    parser.add_argument(
        "-a", "--accel_units", nargs="?", choices=["g", "si"], default="g"
    )
    parser.add_argument(
        "-g",
        "--gyro_units",
        nargs="?",
        choices=["radians", "degrees"],
        default="degrees",
    )
    args = parser.parse_args()
    return args


def convert_degrees_to_radians(degrees):
    return (float(degrees) * math.pi) / 180


def convert_g_to_si(g):
    # Local gravity of Rapid City, SD
    local_gravity = 9.803443
    return float(g) * local_gravity


if __name__ == "__main__":
    """ 
    Reads and prints serial output 
    """
    args = get_arguments()
    try:
        # Use the Conn class to manage a serial connection to the IMU
        ser = Serial(args.comport, args.baudrate)
        conn = Conn(ser, args.format)

        while True:
            # Waits for a list of formatted packets
            packets = conn.read()

            # Verify a packet was received and them print out the value
            if packets:
                for packet in packets:
                    for sub_packet in packet.sub_packets:

                        # Convert degrees to radians
                        if (
                            sub_packet.message_code == cip.ImuOutputCode.Gyro
                            and args.gyro_units == "radians"
                        ):
                            for idx, value in enumerate(sub_packet.values):
                                sub_packet.values[idx] = convert_degrees_to_radians(
                                    value
                                )

                        # Convert g force to si
                        if (
                            sub_packet.message_code == cip.ImuOutputCode.Accel
                            and args.accel_units == "si"
                        ):
                            for idx, value in enumerate(sub_packet.values):
                                sub_packet.values[idx] = convert_g_to_si(value)

                    print(packet)

    except KeyboardInterrupt:
        print("Interrupt received, stopping…")
