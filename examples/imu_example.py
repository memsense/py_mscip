"""
An example showing how to use the IMU class to get and set values on a connected
IMU. 
"""

import argparse
from py_mscip.interfaces.conn import Conn
from py_mscip.interfaces.imu import IMU
from py_mscip.packets import cip
from serial import Serial


def get_arguments():
    """ 
    Get arguments from the command line 
    """
    parser = argparse.ArgumentParser(description="<ctl-c> to exit.")
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    """ 
    Reads and prints the current IMU settings 
    """
    args = get_arguments()

    try:
        # Use the Conn class to manage an serial connection for the IMU
        ser = Serial(args.comport, args.baudrate)
        imu = IMU(ser)

        # Get current settings and information from the IMU
        print(imu.ping())
        print(imu.get_model())
        print(imu.get_sn())
        print(imu.get_fw())
        print(imu.get_cal_date())
        imu.enable_trigger()
        imu.enable_data()
        print(imu.get_select_sensors())

        # Set values on the IMU
        set = input("Would you like to set the IMU values? Y/n: ")
        if set == "Y":
            imu.set_baudrate(460800)
            imu.set_filter(cip.FilterCode.F25)
            imu.set_accel(cip.AccelCode.A2)
            imu.set_gyro(cip.GyroCode.G200)
            imu.set_decimation(1)
    except Exception as e:
        print(e)
