"""
An example showing how to create a plot that updates as new IMU values are read
over the serial connection.
"""

import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from io import StringIO
from py_mscip.interfaces.conn import Conn
import argparse
from serial import Serial


def init():
    """
    Initializes the plot with empty values 
    """
    x_gyro_line.set_data([], [])
    y_gyro_line.set_data([], [])
    z_gyro_line.set_data([], [])
    return x_gyro_line, y_gyro_line, z_gyro_line


def update(i):
    """ 
    Repeatedly called to update the plot with new values 
    """
    global conn
    packets = conn.read()
    if packets:
        for packet in packets:
            print(packet)
            df = pd.read_csv(
                StringIO(str(packet)),
                delim_whitespace=True,
                names=["x-gyro", "y-gyro", "z-gyro", "x-acc", "y-acc", "z-acc", "temp"],
            )
            row = df.loc[:, ["x-gyro", "y-gyro", "z-gyro"]]

            x_gyro_data.append(row["x-gyro"][0])
            if len(x_gyro_data) > 200:
                x_gyro_data.pop(0)

            y_gyro_data.append(row["y-gyro"][0])
            if len(y_gyro_data) > 200:
                y_gyro_data.pop(0)

            z_gyro_data.append(row["z-gyro"][0])
            if len(z_gyro_data) > 200:
                z_gyro_data.pop(0)

            if len(xdata) < 200:
                xdata.append(i)

            x_gyro_line.set_data(xdata, x_gyro_data)
            y_gyro_line.set_data(xdata, y_gyro_data)
            z_gyro_line.set_data(xdata, z_gyro_data)
            return x_gyro_line, y_gyro_line, z_gyro_line


def get_arguments():
    """ 
    Handles program arguments 
    """
    parser = argparse.ArgumentParser(description="<ctl-c> to exit.")
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    parser.add_argument(
        "-f", "--format", nargs="+", choices=["hex", "info"], default="info"
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    """ 
    Plots an updating graph using serial output 
    """
    args = get_arguments()
    fig = plt.figure()

    ax = plt.axes(xlim=(0, 200), ylim=(-500, 500))

    xdata = []
    x_gyro_data = []
    y_gyro_data = []
    z_gyro_data = []
    (x_gyro_line,) = ax.plot(xdata, x_gyro_data, lw=3)
    (y_gyro_line,) = ax.plot(xdata, y_gyro_data, lw=3)
    (z_gyro_line,) = ax.plot(xdata, z_gyro_data, lw=3)

    ser = Serial(args.comport, args.baudrate)
    conn = Conn(ser, args.format)

    anim = FuncAnimation(
        fig, update, init_func=init, frames=200, interval=20, blit=True
    )
    plt.xlabel("time")
    plt.ylabel("deg/sec")
    plt.title("Gyros")
    plt.show()
