from serial import Serial, SerialException
from argparse import ArgumentParser
from cip_parser import Parser
import cip
import packet_factory as pf
import sys
from time import sleep
from threading import Thread
from queue import Queue, Empty
import base_commands as bc
import imu_commands as ic
import json

"""
This script has a number of ways of going through the possible baudrates and trying them all in order to achieve
and auto connect interface to an IMU at a specific port.
"""

DATATYPE_MAP = {
    "Accel": cip.ImuOutputCode.Accel,
    "Gyro": cip.ImuOutputCode.Gyro,
    "Mag": cip.ImuOutputCode.Mag,
    "DeltaTheta": cip.ImuOutputCode.DeltaTheta,
    "DeltaVelocity": cip.ImuOutputCode.DeltaVelocity,
    "TimeStamp": cip.ImuOutputCode.Time,
    "Temp": cip.ImuOutputCode.Temperature,
}

BAUDRATES = (9600, 19200, 57600, 115200, 230400, 460800, 921600)
# BAUDRATES = (460800,)
CS_COUNT = 0


def get_args():
    parser = ArgumentParser()
    parser.add_argument("comport")
    # parser.add_argument('baudrate')
    return parser.parse_args()


def reader(ser, q):
    parser = Parser()
    try:
        while True:
            if ser.in_waiting > 0:
                data = ser.read(ser.in_waiting)
                packets = parser.parse(data)
                for p in packets:
                    if cip.verify_checksum(p):
                        packet = pf.create_packet(bytearray(p))
                        if packet.packet_type == pf.MsgType.ImuOutput:
                            pass
                        else:
                            q.put(packet)
                    else:
                        pass
                sleep(0)
    except:
        # TODO find a better way to stop thread
        pass


def connect(comport, baudrate):
    try:
        sleep(0.05)
        with Serial(comport, baudrate) as ser:
            ser.flushInput()
            # print(f'Trying {baudrate}...  ',end='')
            q = Queue(maxsize=0)
            pinger = Thread(target=reader, args=(ser, q))
            pinger.start()
            ser.write(bc.ping().bytes)
            packet = q.get(block=True, timeout=0.5)
            if packet.bytes == bc.ping_reply().bytes:
                return True, "Yes!!!!"
            else:
                print(f"!!!!!!! ({baudrate})  {packet.bytes}")
                return False, f"Nope. wrong reply  {packet.bytes}"

    except Empty:
        return False, "Nope."


# def ping(comport, baudrate):

#     for br in wrong_rates:
#         status, msg = connect(comport, br)
#         if status:
#             ret[br] += 1
#         status, msg = connect(comport, baudrate)
#         if status:
#             ret[baudrate] += 1
#     return ret


def test(comport, baudrate):
    ret = {9600: 0, 19200: 0, 57600: 0, 115200: 0, 230400: 0, 460800: 0, 921600: 0}
    wrong_rates = [x for x in BAUDRATES if x != baudrate]
    # correct_dic
    for br in wrong_rates:
        status, msg = connect(comport, br)
        # print(f'{br}: {msg}')
        if status:
            ret[br] += 1
        status, msg = connect(comport, baudrate)
        # print(f'{baudrate}: {msg}')
        if status:
            ret[baudrate] += 1
        status, msg = connect(comport, baudrate)
        # print(f'{baudrate}: {msg}')

    return ret


def auto_connect(comport):
    print("Connecting ", end="", flush=True)
    connected = False
    for baudrate in BAUDRATES:
        try:
            print(".", end="", flush=True)
            sleep(0.05)
            with Serial(comport, baudrate) as ser:
                q = Queue(maxsize=0)
                pinger = Thread(target=reader, args=(ser, q))
                pinger.start()
                ser.write(bc.ping().bytes)
                packet = q.get(block=True, timeout=0.2)
                if packet.bytes == bc.ping_reply().bytes:
                    connected = True
                    break
                else:
                    pass
        except Empty:
            pass
    if connected:
        print(f"Connected at {baudrate}")
    else:
        raise Exception("Couldn't connect")
    return baudrate


def setup(comport, config):
    current_baudrate = auto_connect(comport)
    print(f"Connected at {current_baudrate}.")

    with Serial(comport, current_baudrate) as ser:
        ser.write(ic.configure_decimation(int(config["decimation"])).bytes)
        print(f'Set Decimation to {config["decimation"]}')
        data_types = [DATATYPE_MAP[dt] for dt in config["data_types"]]
        ser.write(ic.configure_select_sensors(data_types).bytes)
        print(f'DataTypes:  {config["data_types"]}')
        ser.write(ic.configure_baudrate(int(config["baudrate"])).bytes)
        print(f'Set Baudrate to {config["baudrate"]}')
        print()


def test_setup(comport, test_name, config):
    print(f"Test: {test_name}")
    print("==============")
    current_baudrate = auto_connect(comport)
    print("Configuring...")
    setup(comport, config)

    for i in range(10):
        ret = test(comport, int(config["baudrate"]))
        print(ret)
    print()
    print()


def test_runner(comport):
    with open("baudrate.json") as fin:
        tests = json.load(fin)

    for test in tests:
        test_setup(comport, test, tests[test])


if __name__ == "__main__":
    args = get_args()
    try:
        # auto_connect(args.comport)
        # for baudrate in BAUDRATES:
        # num=100
        # for i in range(num):
        #     ret = test(args.comport, 115200)
        # sleep(0.1)
        # ret, status = connect(args.comport, 460800)
        # print(ret)
        # ret, status = connect(args.comport, 460800)
        # print(status)
        # ret, status = connect(args.comport, 9600)
        # print(status)
        # setup(args.comport, args.baudrate)
        test_runner(args.comport)
    except SerialException as se:
        print(f"Couldn't connect to {args.comport}.")
    except KeyboardInterrupt:
        pass
