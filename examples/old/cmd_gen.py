import packet
import cip
import imu_commands as ic
from enum import Enum
import py_mscip.packet as p

"""
This script is for outputing the hex representations of packets creating using the library.
"""


def print_hex_corrupted(desc, msg, reply):
    print(desc, end=",")
    corrupted = msg.bytes
    corrupted[-1] += 5
    print(cip.format_hex(corrupted).replace(" ", "").upper(), end=",")
    print(cip.format_hex(reply.bytes).replace(" ", "").upper())


def print_hex(desc, msg, reply):
    print(desc, end=",")
    print(cip.format_hex(msg.bytes).replace(" ", "").upper(), end=",")
    print(cip.format_hex(reply.bytes).replace(" ", "").upper())


class Invalid(Enum):
    Invalid = 99


if __name__ == "__main__":

    sensors = [
        cip.ImuOutputCode.Accel,
        cip.ImuOutputCode.Gyro,
        cip.ImuOutputCode.Temperature,
    ]
    desc = "SetDatatypes"
    msg = ic.configure_select_sensors(sensors)
    reply = ic.configure_select_sensors_reply()
    print_hex(desc, msg, reply)

    desc = "GetDatatypes"
    msg = ic.get_select_sensors()
    reply = ic.get_select_sensors_reply(sensors)
    print_hex(desc, msg, reply)
