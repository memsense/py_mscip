import time
import set_gyros as sg
from serial import Serial
from cip_parser import Parser
import packet_factory as pf
from cip import MsgType, BaseMsgCode, ImuOutputCode, ImuMsgCode, compute_fletcher
from packet import Packet, Floats, Ack
from packet_factory import base_lookup, imu_lookup
import numpy as np

"""
This script configures an IMU devices to output gyro measurements and records a specific number of values to a file. The values are then divided by 
the population distributution and printed to the terminal.
"""


def verify_checksum(raw_data):
    computed_fletcher = compute_fletcher(raw_data[:-2])
    raw_data_fletcher = (raw_data[-2] << 8) + raw_data[-1]
    return computed_fletcher == raw_data_fletcher


def create(idx):
    ret = []
    for i in range(18):
        if i in idx:
            ret.append(0)
        else:
            ret.append(1)
    return ret


names = [
    "GX0",
    "GX1",
    "GX2",
    "GX3",
    "GX4",
    "GX5",
    "GY0",
    "GY1",
    "GY2",
    "GY3",
    "GY4",
    "GY5",
    "GZ0",
    "GZ1",
    "GZ2",
    "GZ3",
    "GZ4",
    "GZ5",
    "NA1",
    "NA2",
    "AX",
    "AY",
    "AZ",
    "NA",
    "T",
]


def print_array(arr):
    for a in arr:
        print(f"{a:0.5f}", end=" ")
    print()


def main():
    fp = open("out.txt", "w")
    ser = Serial("COM3", 921600)
    parser = Parser()
    for i in range(18):
        cs = create([i])
        # print(*cs)
        ser.write(sg.configure_select_sensors(cs).bytes)
        time.sleep(1.0)
        ser.flushInput()
        packet_count = 0
        samples = []
        while packet_count < 1000:
            if ser.in_waiting > 0:
                data = ser.read(ser.in_waiting)
                packets = parser.parse(data)
                for packet in packets:
                    if verify_checksum(packet):
                        if packet[3] == 0x66:
                            packet_count += 1
                            p = pf.create_packet(bytearray(packet))
                            # print(*(p.sub_packets[0].values))
                            samples.append(p.sub_packets[0].values)
                            fp.write(str(p.sub_packets[0].values) + "\n")

        a = np.array(samples)
        gyros = a[:, :18]
        print_array(np.std(gyros, axis=0) / np.sqrt(50))


if __name__ == "__main__":
    main()
