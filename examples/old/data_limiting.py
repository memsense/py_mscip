import json
import baudrate as br
from argparse import ArgumentParser

"""
This script reads test values from a file and then sets up the IMU to use
each of the test configs read from the file.
"""


DATATYPE_SIZE_MAP = {
    "Gyro": 14,
    "Accel": 14,
    "DeltaVelocity": 14,
    "DeltaTheta": 14,
    "Mag": 14,
    "TimeStamp": 14,
    "Temp": 6,
}


def get_msg_size(config):
    size = 0
    for data_type in config["data_types"]:
        size += DATATYPE_SIZE_MAP[data_type]
    return size + 6


def percent_bandwidth(config):
    isr = 800
    dec = config["decimation"]
    baudrate = config["baudrate"]
    msg_size = get_msg_size(config)
    return ((isr / dec) * 10 * msg_size) / baudrate * 100


def get_args():
    parser = ArgumentParser()
    parser.add_argument("test")
    return parser.parse_args()


args = get_args()

with open("data_limiting.json") as fin:
    tests = json.load(fin)
config = tests[args.test]
print(f"Message Size:    {get_msg_size(config)} bytes")
print(f"Bandwidth Usage: {percent_bandwidth(config):.2f}%")
print()
br.setup("com3", config)
