import packet_factory
from argparse import ArgumentParser

"""
This script decodes a passed in message. i.e. goes from hex to a library packet.
"""


def get_args():
    parser = ArgumentParser()
    parser.add_argument("message")
    return parser.parse_args()


def decode(message):
    return packet_factory.create_packet(bytearray.fromhex(message))


if __name__ == "__main__":
    args = get_args()
    print(decode(args.message))
