import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

"""
This script plots the measurements read from an IMU device.
"""

df = pd.read_csv(
    "test.txt",
    names=["GX", "GY", "GZ", "AX", "AY", "AZ", "T", "F", "TS"],
    delim_whitespace=True,
)
# df = pd.read_csv('cip_test.txt', names=['GX','GY','GZ','AX','AY','AZ','T'], delim_whitespace=True)

print(df[df["F"] == 5])
plt.figure()
plt.plot(df.loc[:, "GX":"GZ"])
plt.legend(["X", "Y", "Z"])
plt.grid(True)
plt.ylabel("deg/sec")
plt.title("Gyros")

plt.figure()
plt.plot(df.loc[:, "AX":"AZ"])
plt.legend(["X", "Y", "Z"])
plt.title("Accels")
plt.ylabel("m/sec^2")
plt.grid(True)

plt.figure()
plt.plot(df.loc[:, "T"])
plt.title("Temp")
plt.ylabel("deg C")
plt.grid(True)

plt.figure()
plt.plot(df.loc[:, "TS"], "o")
plt.title("Time")
plt.ylabel("nanosecond")
plt.grid(True)

plt.figure()
plt.plot(np.diff(df.loc[:, "TS"] / 1000), "o")
plt.title("Diff")
plt.ylabel("microsecond")
plt.grid(True)


plt.figure()
plt.plot(df.loc[:, "F"], "o")
plt.title("Flag")
plt.grid(True)

print(*df.loc[:, "AX":"AZ"].mean())
plt.show()
