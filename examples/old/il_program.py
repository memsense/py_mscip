import argparse
import sys
from serial import SerialException, Serial
from il_parser import Parser
from cip import compute_fletcher
import packet_factory
import datetime


"""
This script reads values from the IMU and saves them to two files. One in a hex format and one in a float format.
"""


def format_hex(raw_data):
    return " ".join(format(p, "02x") for p in raw_data)


def format_floats(floats):
    return " ".join(format(p, "f") for p in floats)


def get_arguments():
    parser = argparse.ArgumentParser(description="<ctl-c> to exit.")
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    parser.add_argument(
        "-f", "--format", nargs="+", choices=["hex", "info"], default="hex"
    )
    args = parser.parse_args()
    return args


def get_data(packet):
    gx = int.from_bytes(packet[6:10], byteorder="little", signed=True) * 1e-5
    gy = int.from_bytes(packet[10:14], byteorder="little", signed=True) * 1e-5
    gz = int.from_bytes(packet[14:18], byteorder="little", signed=True) * 1e-5

    ax = int.from_bytes(packet[18:22], byteorder="little", signed=True) * 9.8106e-6
    ay = int.from_bytes(packet[22:26], byteorder="little", signed=True) * 9.8106e-6
    az = int.from_bytes(packet[26:30], byteorder="little", signed=True) * 9.8106e-6

    f = int.from_bytes(packet[30:32], byteorder="little", signed=True)
    t = int.from_bytes(packet[36:38], byteorder="little", signed=True) * 0.1

    ts = int.from_bytes(packet[32:36], byteorder="little", signed=True)

    return [gx, gy, gz, ax, ay, az, t, f, ts]


def calculate_checksum(packet):
    return sum(packet[2:38])


def verify_checksum(raw_data):
    calc_cs = calculate_checksum(raw_data)
    cs = (raw_data[39] << 8) + raw_data[38]
    return calc_cs == cs


def verify_packet():
    pass


try:
    cs_count = 0
    valid_count = 0
    args = get_arguments()
    ser = Serial(args.comport, args.baudrate)
    parser = Parser()
    start_time = datetime.datetime.now()
    fp_eu = open("test.txt", "w")
    fp_hex = open("hex.txt", "w")

    while True:
        if ser.in_waiting > 0:
            data = ser.read(ser.in_waiting)

            packets = parser.parse(data)
            for packet in packets:
                if verify_checksum(packet):
                    valid_count += 1
                    if "hex" in args.format:
                        # print(format_hex(packet))
                        # print(format_floats(get_data(packet)))
                        fp_eu.write(format_floats(get_data(packet)) + "\n")
                        fp_hex.write(format_hex(packet) + "\n")
                    if "info" in args.format:
                        print(packet_factory.create_packet(bytearray(packet)))
                else:
                    sys.stderr.write("CS - ERROR: ")
                    sys.stderr.write(format_hex(packet) + "\n")
                    cs_count += 1
    ser.close()

except SerialException:
    print(f"Error: Couldn't open {args.comport}")
except KeyboardInterrupt:
    ser.close()
    fp_eu.close()
    fp_hex.close()
    sys.stderr.write("\n")
    sys.stderr.write("Packets received: " + str(valid_count) + "\n")
    sys.stderr.write("Checksum Errors: " + str(cs_count) + "\n")
    dt = (datetime.datetime.now() - start_time).total_seconds()
    sys.stderr.write("Time: " + str(dt) + " seconds\n")
    sys.stderr.write("Sample Rate: " + format(valid_count / dt, "4.3f") + "hz\n")
    sys.exit()
