import packet
import cip
import imu_commands as ic
from enum import Enum
import py_mscip.packet as p

"""
This script creates a number of error packets and error reply packets and prints the response.
"""


def print_hex_corrupted(desc, msg, reply):
    print(desc, end=",")
    corrupted = msg.bytes
    corrupted[-1] += 5
    print(cip.format_hex(corrupted).replace(" ", "").upper(), end=",")
    print(cip.format_hex(reply.bytes).replace(" ", "").upper())


def print_hex(desc, msg, reply):
    print(desc, end=",")
    print(cip.format_hex(msg.bytes).replace(" ", "").upper(), end=",")
    print(cip.format_hex(reply.bytes).replace(" ", "").upper())


class Invalid(Enum):
    Invalid = 99


if __name__ == "__main__":
    desc = "InvalidFilterParam"
    msg = ic.configure_filter(99)
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.Filter, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidFilterFun"
    msg = p.Packet(cip.MsgType.Imu)
    msg.add_subpacket(
        p.ConfigByte(cip.ImuMsgCode.Filter, Invalid.Invalid, cip.FilterCode.F100)
    )
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.Filter, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidAccelParam"
    msg = ic.configure_accel(99)
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.ConfigAccelRange, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidAccelFun"
    msg = p.Packet(cip.MsgType.Imu)
    msg.add_subpacket(
        p.ConfigByte(cip.ImuMsgCode.ConfigAccelRange, Invalid.Invalid, 0x01)
    )
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.ConfigAccelRange, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidGyroParam"
    msg = ic.configure_gyro(99)
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.ConfigGyroRange, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidGyroFun"
    msg = p.Packet(cip.MsgType.Imu)
    msg.add_subpacket(
        p.ConfigByte(cip.ImuMsgCode.ConfigGyroRange, Invalid.Invalid, 0x01)
    )
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.ConfigGyroRange, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidDataOnOff"
    msg = ic.configure_data_on_off(99)
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.DataOnOff, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidDataOnOffFun"
    msg = p.Packet(cip.MsgType.Imu)
    msg.add_subpacket(p.ConfigByte(cip.ImuMsgCode.DataOnOff, Invalid.Invalid, 0x01))
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.DataOnOff, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidTriggerOnOff"
    msg = ic.configure_trigger_on_off(99)
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.TriggerOnOff, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidTriggerFun"
    msg = p.Packet(cip.MsgType.Imu)
    msg.add_subpacket(p.ConfigByte(cip.ImuMsgCode.TriggerOnOff, Invalid.Invalid, 0x01))
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.TriggerOnOff, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidBaudrate"
    msg = ic.configure_baudrate(99)
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.BaudRate, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidBaudFun"
    msg = p.Packet(cip.MsgType.Imu)
    msg.add_subpacket(p.ConfigInt(cip.ImuMsgCode.BaudRate, Invalid.Invalid, 115200))
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.BaudRate, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidDataType"
    msg = ic.configure_select_sensors([cip.ImuOutputCode.Accel, Invalid.Invalid])
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.SelectSensorsRevB, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidDataTypeFun"
    msg = p.Packet(cip.MsgType.Imu)
    data_types = [cip.ImuOutputCode.Accel]
    msg.add_subpacket(
        p.ConfigListOfBytes(
            cip.ImuMsgCode.SelectSensorsRevB, Invalid.Invalid, data_types
        )
    )
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.SelectSensorsRevB, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidLegDataType"
    msg = ic.configure_select_sensors_old([cip.ImuOutputCode.Accel, Invalid.Invalid])
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.SelectSensorsOld, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidLegDataTypFun"
    msg = p.Packet(cip.MsgType.Imu)
    data_types = [cip.ImuOutputCode.Accel]
    msg.add_subpacket(
        p.ConfigListOfBytes(
            cip.ImuMsgCode.SelectSensorsOld, Invalid.Invalid, data_types
        )
    )
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.SelectSensorsOld, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "ChecksumError"
    msg = ic.configure_baudrate(99)
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(packet.Ack(cip.ImuMsgCode.BaudRate, cip.AckCode.ErrChecksum))
    print_hex_corrupted(desc, msg, reply)

    desc = "MessageTypeError"
    msg = p.Packet(Invalid.Invalid)
    msg.add_subpacket(
        p.ConfigInt(cip.ImuMsgCode.BaudRate, cip.FunctionCode.Use, 115200)
    )
    reply = packet.Packet(Invalid.Invalid)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.BaudRate, cip.AckCode.ErrUnknownStructure)
    )
    print_hex(desc, msg, reply)

    desc = "ImuMessageCodeError"
    msg = p.Packet(cip.MsgType.Imu)
    msg.add_subpacket(p.ConfigInt(Invalid.Invalid, cip.FunctionCode.Use, 115200))
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(packet.Ack(Invalid.Invalid, cip.AckCode.ErrUnknownField))
    print_hex(desc, msg, reply)

    desc = "BaseMessageCodeError"
    msg = p.Packet(cip.MsgType.Base)
    msg.add_subpacket(p.ConfigInt(Invalid.Invalid, cip.FunctionCode.Use, 115200))
    reply = packet.Packet(cip.MsgType.Base)
    reply.add_subpacket(packet.Ack(Invalid.Invalid, cip.AckCode.ErrUnknownField))
    print_hex(desc, msg, reply)

    desc = "InvalidDecFun"
    msg = p.Packet(cip.MsgType.Imu)
    msg.add_subpacket(p.ConfigShort(cip.ImuMsgCode.Decimation, Invalid.Invalid, 10))
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.Decimation, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)

    desc = "InvalidConfigAllFun"
    msg = ic.config_all(cip.FunctionCode.Use)
    reply = packet.Packet(cip.MsgType.Imu)
    reply.add_subpacket(
        packet.Ack(cip.ImuMsgCode.ConfigAll, cip.AckCode.ErrInvalidParameter)
    )
    print_hex(desc, msg, reply)
