from argparse import ArgumentParser
from time import sleep
import base_commands as bc
import imu_commands as ic
from serial import Serial
from cip_parser import Parser
import packet_factory as pf
import colorama
from threading import Thread
from queue import Queue
import sys
import cip
import datetime

"""
Script to asynchronously ping and read replies from an IMU device.
"""

color = {
    pf.MsgType.ImuOutput: colorama.Fore.MAGENTA,
    pf.MsgType.Base: colorama.Fore.GREEN,
    pf.MsgType.Imu: colorama.Fore.CYAN,
}


def ping_thread(ser, q):
    send(ser, bc.ping())
    while True:
        while q.empty():
            pass
        # if packet.type
        send(ser, bc.ping())
        packet = q.get()
        print(f"{ color[packet.packet_type]}{packet}{colorama.Style.RESET_ALL}")
        # print('Read from queue')


def format_hex(raw_data):
    return " ".join(format(p, "02x") for p in raw_data)


def verify_checksum(raw_data):
    computed_fletcher = cip.compute_fletcher(raw_data[:-2])
    raw_data_fletcher = (raw_data[-2] << 8) + raw_data[-1]
    if computed_fletcher != raw_data_fletcher:
        print(
            f'[{format_hex(computed_fletcher.to_bytes(2,"big"))}] [{format_hex(raw_data_fletcher.to_bytes(2,"big"))}]'
        )
    return computed_fletcher == raw_data_fletcher


def get_args():
    parser = ArgumentParser()
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    parser.add_argument("--decimation", action="store", type=int)
    return parser.parse_args()


def send(ser, cmd):
    print(f"{colorama.Fore.YELLOW}{cmd}{colorama.Style.RESET_ALL}")
    ser.write(cmd.bytes)
    ser.flush()


def main(comport, baudrate):
    try:
        colorama.init()

        ser = Serial(args.comport, args.baudrate)
        parser = Parser()
        valid_count = 0
        cs_count = 0

        send(ser, ic.configure_decimation(1))
        sleep(1)
        data = ser.read(ser.in_waiting)
        q = Queue(maxsize=0)
        pinger = Thread(target=ping_thread, args=(ser, q))
        pinger.setDaemon(True)
        pinger.start()
        while True:
            if ser.in_waiting > 0:
                data = ser.read(ser.in_waiting)
                packets = parser.parse(data)
                for p in packets:
                    if verify_checksum(p):
                        packet = pf.create_packet(bytearray(p))
                        if packet.packet_type == pf.MsgType.ImuOutput:
                            valid_count += 1
                            # print(packet)
                            # sys.stderr.write(format_hex(p) + '\n')
                            # if not (valid_count % 1):
                            #     send(ser, bc.ping())
                        else:

                            q.put(packet)
                    else:
                        sys.stderr.write("CS - ERROR: ")
                        sys.stderr.write(format_hex(p) + "\n")
                        cs_count += 1
                        # sys.exit(1)
                sleep(0)
    except Exception as e:
        print(e)
    finally:
        return valid_count, cs_count


if __name__ == "__main__":

    args = get_args()
    start_time = datetime.datetime.now()
    valid_count, cs_count = main(args.comport, args.baudrate)

    print(colorama.Style.RESET_ALL)
    sys.stderr.write("\n")
    sys.stderr.write("Packets received: " + str(valid_count) + "\n")
    sys.stderr.write("Checksum Errors: " + str(cs_count) + "\n")
    dt = (datetime.datetime.now() - start_time).total_seconds()
    sys.stderr.write("Sample Rate: " + format(valid_count / dt, "4.1f") + "hz\n")
    sys.exit()
