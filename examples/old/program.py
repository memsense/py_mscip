import argparse
import sys
from serial import SerialException, Serial
from cip_parser import Parser
from cip import compute_fletcher
import packet_factory
import datetime

"""
Reads IMU values and prints them to the terminal.
"""


def format_hex(raw_data):
    return " ".join(format(p, "02x") for p in raw_data)


def format_floats(floats):
    return " ".join(format(p, "f") for p in floats)


def get_arguments():
    parser = argparse.ArgumentParser(description="<ctl-c> to exit.")
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    parser.add_argument(
        "-f", "--format", nargs="+", choices=["hex", "info"], default="info"
    )
    parser.add_argument("-c", action="store", dest="count", type=int)
    args = parser.parse_args()
    return args


def verify_checksum(raw_data):
    computed_fletcher = compute_fletcher(raw_data[:-2])
    raw_data_fletcher = (raw_data[-2] << 8) + raw_data[-1]
    return computed_fletcher == raw_data_fletcher


try:
    cs_count = 0
    valid_count = 0
    args = get_arguments()
    ser = Serial(args.comport, args.baudrate)
    parser = Parser()
    start_time = datetime.datetime.now()
    while True:
        if ser.in_waiting > 0:
            data = ser.read(ser.in_waiting)
            packets = parser.parse(data)
            for packet in packets:
                if verify_checksum(packet):
                    valid_count += 1
                    if "hex" in args.format:
                        print(format_hex(packet))
                    if "info" in args.format:
                        print(packet_factory.create_packet(bytearray(packet)))
                else:
                    sys.stderr.write("CS - ERROR: ")
                    sys.stderr.write(format_hex(packet) + "\n")
                    cs_count += 1
        if not args.count == None:
            if valid_count >= args.count:
                break

    ser.close()

except SerialException:
    print(f"Error: Couldn't open {args.comport}")
except KeyboardInterrupt:
    sys.stderr.write("\n")
    sys.stderr.write("Packets received: " + str(valid_count) + "\n")
    sys.stderr.write("Checksum Errors: " + str(cs_count) + "\n")
    dt = (datetime.datetime.now() - start_time).total_seconds()
    sys.stderr.write("Time: " + str(dt) + " seconds\n")
    sys.stderr.write("Sample Rate: " + format(valid_count / dt, "4.3f") + "hz\n")
    sys.stderr.write(str(args.count))
    sys.exit()
