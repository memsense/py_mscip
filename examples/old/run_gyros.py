import os
from time import sleep
from itertools import combinations
from datetime import datetime

"""
Prints all possible chip select combinations
"""

start_time = datetime.now()
comport = "com3"
baudrate = "921600"


chipselects = [
    "00",
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
]
combos = list(combinations(chipselects, 5))
print(f"{len(combos)} combinations.")
for x in combos:
    gyros = "_".join(x)
    print(gyros)
    outfile = f"c:\\wd\\python\\3011_tdk\\gyros_{gyros}.txt"
    # outfile = 'c:\\wd\\python\\3011_tdk\\test.txt'
    os.system(f"set_gyros.py {comport} {baudrate} {gyros}")

    os.system(f"program.py  {comport} {baudrate} -c200 > {outfile}")

print(f"{len(combos)} combinations.")
print(datetime.now() - start_time)
