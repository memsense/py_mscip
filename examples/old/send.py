from serial import Serial, SerialException
from time import sleep
from packet import Packet, Floats
from threading import Thread
from cip import ImuOutputCode, MsgType, compute_fletcher, format_hex, BaseMsgCode, ImuMsgCode
import numpy as np    
from argparse import ArgumentParser
from queue import Queue
from cip_parser import Parser
import datetime
import sys
import packet_factory
import base_commands as bc
import imu_commands as ic
import json

"""
Listens for IMU packets and then sends a reply
"""

def get_arguments():
    parser = ArgumentParser(description="<ctl-c> to exit.")
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    parser.add_argument('-f','--format', nargs='+', choices=['hex','info'], default='hex')
    args = parser.parse_args()
    return args

def verify_checksum(raw_data):
    computed_fletcher = compute_fletcher(raw_data[:-2])
    raw_data_fletcher = (raw_data[-2] << 8) + raw_data[-1]
    return computed_fletcher == raw_data_fletcher

def send(ser, queue):
	t=0
	while True:
		t += 0.06
		a = np.sin(t)
		b = 0.5*np.cos(t)
		c = 0.25*np.sin(2*t)
		packet = Packet(MsgType.ImuOutput)
		packet.add_subpacket(Floats(ImuOutputCode.Accel, [a, b, c]))
		packet.add_subpacket(Floats(ImuOutputCode.Gyro, [100*a, 100*b, 100*c]))
		ser.write(packet.bytes)
		sleep(0.02)



try:
    with open('12345.json') as f:
        settings = json.load(f)
    cs_count = 0
    valid_count = 0
    args = get_arguments()
    ser = Serial(args.comport, args.baudrate)
    parser = Parser()
    q = Queue(maxsize=0)
    pinger = Thread(target=send, args=(ser, q))
    pinger.setDaemon(True)
    #pinger.start()
    start_time = datetime.datetime.now()
    while True:
        if ser.in_waiting > 0:
            data = ser.read(ser.in_waiting)
            packets = parser.parse(data)
            for packet in packets:
                if verify_checksum(packet): 
                    valid_count += 1
                    if 'hex' in args.format:
                        print(format_hex(packet))
                    if 'info' in args.format:
                        print(packet_factory.create_packet(bytearray(packet)))
                    p = packet_factory.create_packet(bytearray(packet))
                    print(p.sub_packets[0].message_code)
                    if (p.packet_type.value == MsgType.Base.value):
                        if (p.sub_packets[0].message_code.value == BaseMsgCode.GetSn.value):
                            ser.write(bc.get_sn_reply(settings['serial_number']).bytes)
                        elif (p.sub_packets[0].message_code.value == BaseMsgCode.GetModel.value):
                            ser.write(bc.get_model_reply(settings['product']).bytes)
                        elif (p.sub_packets[0].message_code.value == BaseMsgCode.GetFw.value):
                            ser.write(bc.get_fw_reply(settings['fw_version']).bytes)
                        elif (p.sub_packets[0].message_code.value == BaseMsgCode.GetCal.value):
                            ser.write(bc.get_cal_reply(settings['cal_date']).bytes)
                        elif (p.sub_packets[0].message_code.value == BaseMsgCode.GetCmds.value):
                            ser.write(bc.get_cmds_reply(settings['cmds']).bytes)
                    else:
                         if (p.sub_packets[0].message_code.value == ImuMsgCode.GetInternalSampleRate.value):
                            ser.write(ic.get_isr_reply(settings['isr']).bytes)
                         if (p.sub_packets[0].message_code.value == ImuMsgCode.Decimation.value):
                            ser.write(ic.configure_decimation_reply(6).bytes)

                else:
                    sys.stderr.write('CS - ERROR: ')
                    sys.stderr.write(format_hex(packet) + '\n')
                    cs_count += 1
    ser.close()

except SerialException:
    print(f"Error: Couldn't open {args.comport}")
except KeyboardInterrupt:
    sys.stderr.write('\n')
    sys.stderr.write('Packets received: ' + str(valid_count) + '\n')
    sys.stderr.write('Checksum Errors: ' + str(cs_count) + '\n')
    dt = (datetime.datetime.now() - start_time).total_seconds()
    sys.stderr.write('Time: ' + str(dt) + ' seconds\n')
    sys.stderr.write('Sample Rate: ' + format(valid_count / dt, "4.3f") + 'hz\n')
    sys.exit() 