from serial import Serial, SerialException
from argparse import ArgumentParser
import imu_commands as ic


def format_hex(raw_data):
    return " ".join(format(p, "02x") for p in raw_data)


def get_args():
    parser = ArgumentParser()
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    parser.add_argument("is_il")
    return parser.parse_args()
