from serial import Serial, SerialException
from argparse import ArgumentParser
import imu_commands as ic
import time

"""
Sends a packet to set which gyros are enabled.
"""


def format_hex(raw_data):
    return " ".join(format(p, "02x") for p in raw_data)


def get_args():
    parser = ArgumentParser()
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    parser.add_argument("gyros")
    return parser.parse_args()


def parse_gyros(gyros):
    gyros_split = gyros.split("_")

    gyros_int = [int(x) for x in gyros_split]
    ret = []
    for i in range(18):
        if i in gyros_int:
            ret.append(1)
        else:
            ret.append(0)
    return ret


if __name__ == "__main__":
    args = get_args()
    gyros = parse_gyros(args.gyros)
    ser = Serial(args.comport, args.baudrate)
    packet = ic.set_gyros(ic.FunctionCode.Use, gyros)
    # print(format_hex(packet.bytes))
    ser.write(packet.bytes)
    time.sleep(0.1)
    ser.close()
