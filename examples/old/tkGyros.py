import tkinter as tk

# from tkinter import ttk
import set_gyros

"""
Creates a GUI for changing which gyros are enabled.
"""

gyros = []


def send():
    values = map((lambda var: var.get()), gyros)
    set_gyros.send(list(values))


root = tk.Tk()

for gyro in range(6):
    var = tk.IntVar()
    tk.Checkbutton(text=f"GZ{gyro}", variable=var).grid(row=0, column=gyro)
    gyros.append(var)
for gyro in range(6):
    var = tk.IntVar()
    tk.Checkbutton(text=f"GX{gyro}", variable=var).grid(row=1, column=gyro)
    gyros.append(var)
for gyro in range(6):
    var = tk.IntVar()
    tk.Checkbutton(text=f"GY{gyro}", variable=var).grid(row=2, column=gyro)
    gyros.append(var)
for g in gyros:
    g.set(1)


tk.Button(root, text="Send", command=send).grid(row=3, column=3)
root.mainloop()
