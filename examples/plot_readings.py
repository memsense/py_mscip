"""
An example that reads previously saved IMU values from a file and generates 
plots using matplotlib. The IMU data file can be generated using 
save_readings.py.
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# Run save_conn.py first to generate a file with data to plot
if __name__ == "__main__":
    """ 
    Plots data saved to a file 
    """
    plt.close("all")

    data = pd.read_csv(
        "data",
        delim_whitespace=True,
        names=["x-rate", "y-rate", "z-rate", "x-acc", "y-acc", "z-acc", "temp"],
    )

    gyros = data.loc[:, ["x-rate", "y-rate", "z-rate"]].plot()
    gyros.set_title("Gyros")
    gyros.set_ylabel("deg/sec")

    accels = data.loc[:, ["x-acc", "y-acc", "z-acc"]].plot()
    accels.set_title("Accels")
    accels.set_ylabel("g")

    temp = data.loc[:, ["temp"]].plot()
    temp.set_title("Temperature")
    temp.set_ylabel("C")

    plt.show()
