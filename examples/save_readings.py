"""
An example that reads IMU values from a serial connection and saves the values
to a file for later use.
"""

import argparse
from py_mscip.interfaces.conn import Conn
from serial import Serial


def get_arguments():
    """ 
    Handles program arguments
    """
    parser = argparse.ArgumentParser(description="<ctl-c> to exit.")
    parser.add_argument("comport")
    parser.add_argument("baudrate")
    parser.add_argument(
        "-f", "--format", nargs="+", choices=["hex", "info"], default="info"
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    """ 
    Saves serial output to a file 
    """
    args = get_arguments()
    ser = Serial(args.comport, args.baudrate)
    conn = Conn(ser, args.format)

    file = open("data", "w")

    while True:
        packets = conn.read()
        if packets:
            for packet in packets:
                file.write(str(packet) + "\n")

    file.close()
