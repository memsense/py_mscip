FROM python:3.8.2-buster

RUN mkdir /py_mscip
WORKDIR /py_mscip
COPY requirements.txt .

RUN python -m pip install -r requirements.txt