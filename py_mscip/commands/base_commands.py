"""
Commands for creating base packets.
"""

import py_mscip.packets.packet as p
from py_mscip.packets.cip import MsgType, BaseMsgCode, AckCode
from typing import List


def ping() -> p.Packet:
    """
    Generates a ping packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.NoPayload(BaseMsgCode.Ping))
    return packet


def ping_reply() -> p.Packet:
    """
    Generates a ping reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.Ack(BaseMsgCode.Ping, AckCode.OK))
    return packet


def reset() -> p.Packet:
    """
    Generates a reset packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.NoPayload(BaseMsgCode.Reset))
    return packet


def reset_reply() -> p.Packet:
    """
    Generates a reset reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.Ack(BaseMsgCode.Reset, AckCode.OK))
    return packet


def get_bit() -> p.Packet:
    """
    Generates a time data bit request packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.NoPayload(BaseMsgCode.GetBit))
    return packet


def get_bit_reply(bit: bool) -> p.Packet:
    """
    Generates a time data bit reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.Ack(BaseMsgCode.GetBit, AckCode.OK))
    packet.add_subpacket(p.Bit(BaseMsgCode.GetBitReply, bit))
    return packet


def get_cmds() -> p.Packet:
    """
    Generates an available commands request packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.NoPayload(BaseMsgCode.GetCmds))
    return packet


def get_cmds_reply(cmds: List[int]) -> p.Packet:
    """
    Generates an available commands reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.Ack(BaseMsgCode.GetCmds, AckCode.OK))
    packet.add_subpacket(p.GetCmds(BaseMsgCode.GetCmdsReply, cmds))
    return packet


def get_model() -> p.Packet:
    """
    Generates a model request packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.NoPayload(BaseMsgCode.GetModel))
    return packet


def get_model_reply(model_name: str) -> p.Packet:
    """
    Generates a model reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.Ack(BaseMsgCode.GetModel, AckCode.OK))
    packet.add_subpacket(p.GetString(BaseMsgCode.GetModelReply, model_name))
    return packet


def get_sn() -> p.Packet:
    """
    Generates a serial number request packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.NoPayload(BaseMsgCode.GetSn))
    return packet


def get_sn_reply(serial_number: str) -> p.Packet:
    """
    Generates a serial number reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.Ack(BaseMsgCode.GetSn, AckCode.OK))
    packet.add_subpacket(p.GetString(BaseMsgCode.GetSnReply, serial_number))
    return packet


def get_fw() -> p.Packet:
    """
    Generates a firmware version request packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.NoPayload(BaseMsgCode.GetFw))
    return packet


def get_fw_reply(firmware_version: str) -> p.Packet:
    """
    Generates a firmware version reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.Ack(BaseMsgCode.GetFw, AckCode.OK))
    packet.add_subpacket(p.GetString(BaseMsgCode.GetFwReply, firmware_version))
    return packet


def get_cal() -> p.Packet:
    """
    Generates a calibration date request packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.NoPayload(BaseMsgCode.GetCal))
    return packet


def get_cal_reply(cal_date: str) -> p.Packet:
    """
    Generates a calibration date reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.Ack(BaseMsgCode.GetCal, AckCode.OK))
    packet.add_subpacket(p.GetString(BaseMsgCode.GetCalReply, cal_date))
    return packet


def set_time(week: int, second: int) -> p.Packet:
    """
    Generates a GPS time set request packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.SetTime(BaseMsgCode.SetTime, week, second))
    return packet


def set_time_reply() -> p.Packet:
    """
    Generates a GPS time set reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(MsgType.Base)
    packet.add_subpacket(p.Ack(BaseMsgCode.SetTime, AckCode.OK))
    return packet
