"""
Commands for creating IMU packets.
"""

import py_mscip.packets.packet as p
from py_mscip.packets import cip
from typing import List


def get_isr() -> p.Packet:
    """
    Generates an isr request packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(p.NoPayload(cip.ImuMsgCode.GetInternalSampleRate))
    return packet


def get_isr_reply(value: int) -> p.Packet:
    """
    Generates an isr reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(p.Ack(cip.ImuMsgCode.GetInternalSampleRate, cip.AckCode.OK))
    packet.add_subpacket(p.GetShort(cip.ImuMsgCode.GetInternalSampleRateReply, value))
    return packet


def get_select_sensors() -> p.Packet:
    """
    Generates a select sensors request packet.

    Returns:
        packet: A request packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.Request(cip.ImuMsgCode.SelectSensorsRevB, cip.FunctionCode.Request)
    )
    return packet


def get_select_sensors_reply(sensors: List[cip.ImuOutputCode]) -> p.Packet:
    """
    Generates an select sensors reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(p.Ack(cip.ImuMsgCode.SelectSensorsRevB, cip.AckCode.OK))
    packet.add_subpacket(
        p.ReplyListOfBytes(cip.ImuMsgCode.SelectSensorsRevBReply, sensors)
    )
    return packet


def configure_baudrate(baudrate: cip.BaudRateCode) -> p.Packet:
    """
    Generates a baudrate configuration packet.

    Returns:
        packet: A configuration packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.ConfigInt(cip.ImuMsgCode.BaudRate, cip.FunctionCode.Use, baudrate)
    )
    return packet


def configure_baudrate_reply() -> p.Packet:
    """
    Generates a baudrate configuration reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(p.Ack(cip.ImuMsgCode.Filter, cip.AckCode.OK))
    return packet


def configure_filter(filter_code: cip.FilterCode) -> p.Packet:
    """
    Generates a filter configuration packet.

    Returns:
        packet: A configuration packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.ConfigByte(cip.ImuMsgCode.Filter, cip.FunctionCode.Use, filter_code)
    )
    return packet


def configure_filter_reply() -> p.Packet:
    """
    Generates a filter configuration reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(p.Ack(cip.ImuMsgCode.Filter, cip.AckCode.OK))
    return packet


def configure_accel(accel_code: cip.AccelCode) -> p.Packet:
    """
    Generates a accel configuration packet.

    Returns:
        packet: A configuration packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.ConfigByte(cip.ImuMsgCode.ConfigAccelRange, cip.FunctionCode.Use, accel_code)
    )
    return packet


def configure_gyro(gyro_code: cip.GyroCode) -> p.Packet:
    """
    Generates a gyro configuration packet.

    Returns:
        packet: A configuration packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.ConfigByte(cip.ImuMsgCode.ConfigGyroRange, cip.FunctionCode.Use, gyro_code)
    )
    return packet


def configure_decimation(decimation: int) -> p.Packet:
    """
    Generates a decimation configuration packet.

    Returns:
        packet: A configuration packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.ConfigShort(cip.ImuMsgCode.Decimation, cip.FunctionCode.Use, decimation)
    )
    return packet


def configure_data_on_off(enable: cip.EnableCode) -> p.Packet:
    """
    Generates a data configuration packet.

    Returns:
        packet: A configuration packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.ConfigByte(cip.ImuMsgCode.DataOnOff, cip.FunctionCode.Use, enable)
    )
    return packet


def configure_trigger_on_off(enable: cip.EnableCode) -> p.Packet:
    """
    Generates a trigger configuration packet.

    Returns:
        packet: A configuration packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.ConfigByte(cip.ImuMsgCode.TriggerOnOff, cip.FunctionCode.Use, enable)
    )
    return packet


def configure_select_sensors(sensors: List[cip.ImuOutputCode]) -> p.Packet:
    """
    Generates a select sensors configuration packet.

    Returns:
        packet: A configuration packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.ConfigListOfBytes(
            cip.ImuMsgCode.SelectSensorsRevB, cip.FunctionCode.Use, sensors
        )
    )
    return packet


def configure_select_sensors_reply() -> p.Packet:
    """
    Generates a select sensors configuration reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(p.Ack(cip.ImuMsgCode.SelectSensorsRevB, cip.AckCode.OK))
    return packet


def configure_select_sensors_old(sensors: List[cip.ImuOutputCode]) -> p.Packet:
    """
    Generates a select sensors configuration packet.

    Returns:
        packet: A configuration packet.

    Note:
        This has been depreciated for configure_select_sensors.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.ConfigListOfBytes(
            cip.ImuMsgCode.SelectSensorsOld, cip.FunctionCode.Use, sensors
        )
    )
    return packet


def config_all(function_code: cip.FunctionCode) -> p.Packet:
    """
    Generates a save, load, or reset factory settings packet.

    Returns:
        packet: A configuration packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(p.Request(cip.ImuMsgCode.ConfigAll, function_code))
    return packet


def config_all_reply() -> p.Packet:
    """
    Generates a save, load, or reset factory settings reply packet.

    Returns:
        packet: A reply packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(p.Ack(cip.ImuMsgCode.ConfigAll, cip.AckCode.OK))
    return packet


def set_gyros(function_code: cip.FunctionCode, on_off: bool) -> p.Packet:
    """
    Generates a gyros configuration packet.

    Returns:
        packet: A configuration packet.
    """
    packet = p.Packet(cip.MsgType.Imu)
    packet.add_subpacket(
        p.SetListOfBytes(cip.ImuMsgCode.SetGyros, function_code, on_off)
    )

    return packet
