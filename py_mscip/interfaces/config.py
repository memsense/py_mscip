"""
Interface for saving and loading IMU configurations.
"""

import json
from py_mscip.interfaces.imu import IMU
from py_mscip.packets import cip
from typing import Dict, Optional


class Config:
    """
    Handles saving and loading configurations from an IMU.
    """

    #  Stores the device's configuration settings
    _config: Dict[str, str] = {}

    def __init__(self, sn: str = None, model: str = None, fw: str = None):
        """
        Creates object with initial settings.

        Args:
            sn (str, optional): IMU device serial number.
            model (str, optional): IMU model.
            fw (str, optional): IMU firmware version.
        """
        if sn:
            self._config["sn"] = sn
        if model:
            self._config["model"] = model
        if fw:
            self._config["fw"] = fw

    def __str__(self) -> str:
        """
        Converts the current settings to a JSON string.

        Returns:
            str: Settings as a JSON string
        """
        return json.dumps(self._config, indent=4)

    def write(self, imu: IMU) -> None:
        """
        Writes the configs to the connected IMU.

        Args:
            imu (IMU): The connected IMU.
        """
        if "accel" in self._config:
            imu.set_accel(cip.AccelCode(self._config["accel"]))
        if "filter" in self._config:
            imu.set_filter(cip.FilterCode(self._config["filter"]))
        if "gyro" in self._config:
            imu.set_gyro(cip.GyroCode(self._config["gyro"]))
        if "decimation" in self._config:
            imu.set_decimation(int(self._config["decimation"], 16))

    def read(self, imu: IMU) -> None:
        """
        Reads the configs from the connected IMU.

        Args:
            imu (IMU): The connected IMU.
        """
        self._config["sn"] = imu.get_sn()
        self._config["model"] = imu.get_model()
        self._config["fw"] = imu.get_fw()

    def save(self, filename: str) -> None:
        """
        Saves the configs to a JSON file.

        Args:
            filename (str): JSON Filename to save settings to.
        """
        with open(filename, "w") as file:
            json.dump(self._config, file, indent=4)

    def load(self, filename: str) -> None:
        """
        Loads the config from a JSON file.

        Args:
            filename (str): JSON filename to load settings from.
        """
        with open(filename, "r") as file:
            self._config = json.load(file)
