"""
Interface for managing an IMU serial connection.
"""

import sys
import py_mscip.packets.packet as p
from serial import SerialException, Serial  # type: ignore
from py_mscip.packets.cip_parser import Parser
from py_mscip.packets import packet_factory as pf
from typing import List


class Conn:
    """
    Creates and manages a serial connection with an IMU device.
    """

    def __init__(self, ser: Serial, packet_format: str = "info"):
        """
        Initializes a connection with the desired settings.

        Args:
            ser (serial): Serial class for connected IMU.
            format (str, optional): Output format of the parsed packages.
        """
        self.ser = ser
        self.packet_format = packet_format

        self.parser = Parser()

    def read(self) -> List[p.Packet]:
        """
        Reads and formats packets from the serial connection.

        Returns:
            list(packets): A list of formatted packets.
        """
        formatted_packets = []
        try:
            if self.ser.in_waiting > 0:
                data = self.ser.read(self.ser.in_waiting)
                packets = self.parser.parse(data)
                for packet in packets:
                    if p.verify_checksum(packet):
                        if "hex" in self.packet_format:
                            formatted_packet = p.format_hex(packet)
                            formatted_packets.append(formatted_packet)
                        if "info" in self.packet_format:
                            formatted_packet = pf.create_packet(bytearray(packet))
                            formatted_packets.append(formatted_packet)
                    else:
                        sys.stderr.write("CS - ERROR: ")
                        sys.stderr.write(p.format_hex(packet) + "\n")
        except SerialException:
            sys.stderr.write(f"Error: Couldn't open {self.ser.comport}\n")
        return formatted_packets

    def write(self, raw_bytes: bytearray) -> None:
        """
        Writes a packet to the serial connection.

        Args:
            bytes (str): Bytes to be written to the serial connection.
        """
        self.ser.write(raw_bytes)
