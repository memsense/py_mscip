"""
Interface for sending IMU commands.
"""

import concurrent.futures
import time
from serial import Serial  # type: ignore
from py_mscip.interfaces.conn import Conn
from py_mscip.commands import base_commands as bc
from py_mscip.commands import imu_commands as ic
from py_mscip.packets import cip
import py_mscip.packets.packet as p
from typing import List, Callable, Any


class IMU:
    """
    Acts as an interface for communicating with an IMU.
    """

    def __init__(self, ser: Serial):
        self.conn = Conn(ser)

    def ping(self) -> bool:
        """
        Pings the connected IMU.

        Returns:
            bool: True on success, false otherwise.
        """
        reply = self._get(bc.ping)
        return True

    def reset(self) -> None:
        """
        Resets the connected IMU.

        Returns:
            None.
        """
        reply = self._get(bc.reset)
        return None

    ####################################
    #              Getters             #
    ####################################

    def get_bit(self) -> List[int]:
        """
        Requests the time data bit value for the connected IMU.

        Returns:
            int: Currently set bits.
        """
        reply = self._get(bc.get_bit)
        return list(reply.sub_packets[0].payload)

    def get_cmds(self) -> List[int]:
        """
        Requests available commands for the connected IMU.

        Returns:
            int: Available commands for the IMU.
        """

        reply = self._get(bc.get_cmds)
        return list(reply.sub_packets[1].payload)

    def get_model(self) -> str:
        """
        Requests the model of the connected IMU.

        Returns:
            str: Model of IMU.
        """
        reply = self._get(bc.get_model)
        return str(reply.sub_packets[1].payload.decode("utf-8").strip())

    def get_sn(self) -> str:
        """
        Requests the serial number of the connected IMU.

        Returns:
            str: Serial number of IMU.
        """
        reply = self._get(bc.get_sn)
        return str(reply.sub_packets[1].payload.decode("utf-8").strip())

    def get_fw(self) -> str:
        """
        Requests the firmware version of the connected IMU.

        Returns:
            str: Firmware version of IMU.
        """
        reply = self._get(bc.get_fw)
        return str(reply.sub_packets[1].payload.decode("utf-8").strip())

    def get_cal_date(self) -> str:
        """
        Requests the calibration date of the connected IMU.

        Returns:
            str: Calibration date of IMU.
        """
        reply = self._get(bc.get_cal)
        return str(reply.sub_packets[1].payload.decode("utf-8").strip())

    def get_isr(self) -> int:
        """
        Requests the internal sample rate of the connected IMU.

        Returns:
            str: Internal sample rate of IMU.
        """
        reply = self._get(ic.get_isr)
        return int(reply.sub_packets[1].value)

    def get_select_sensors(self) -> List[cip.ImuOutputCode]:
        """
        Requests the selected sensors of the connected IMU.

        Returns:
            str: Enabled sensors of of IMU.
        """
        reply = self._get(ic.get_select_sensors)

        return [(cip.ImuOutputCode(sensor)) for sensor in reply.sub_packets[1].payload]

    ####################################
    #              Setters             #
    ####################################

    def set_baudrate(self, baudrate: cip.BaudRateCode) -> bool:
        """
        Sets the baudrate on the connected IMU.

        Args:
            baudrate (int): Desired baud rate value.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.configure_baudrate(baudrate)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def set_filter(self, imu_filter: cip.FilterCode) -> bool:
        """
        Sets the filter value on the connected IMU.

        Args:
            filter (FilterCode): Desired filter code.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.configure_filter(imu_filter)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def set_accel(self, accel: cip.AccelCode) -> bool:
        """
        Sets the accel value on the connected IMU.

        Args:
            accel (AccelCode): Desired accel code.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.configure_accel(accel)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def set_gyro(self, gyro: cip.GyroCode) -> bool:
        """
        Sets the gyro value on the connected IMU.

        Args:
            gyro (GyroCode): Desired gyro code.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.configure_gyro(gyro)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def set_decimation(self, decimation: int) -> bool:
        """
        Sets the decimation on the connected IMU.

        Args:
            decimation (int): Desired decimation value.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.configure_decimation(decimation)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def set_select_sensors(self, sensors: List[cip.ImuOutputCode]) -> bool:
        """
        Sets the selected sensors on the connected IMU.

        Args:
            sensors (list[ImuOutputCode]): Desired enabled sensors.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.configure_select_sensors(sensors)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def save_startup_settings(self) -> bool:
        """
        Saves the current settings as the startup settings the connected IMU.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.config_all(cip.FunctionCode.Save)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def load_startup_settings(self) -> bool:
        """
        Loads the startup settings as the current settings the connected IMU.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.config_all(cip.FunctionCode.Load)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def reset_startup_settings(self) -> bool:
        """
        Resets the startup settings on the connected IMU.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.config_all(cip.FunctionCode.Reset)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def enable_data(self) -> bool:
        """
        Enables the data output on the connected IMU.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.configure_data_on_off(cip.EnableCode.Enable)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def disable_data(self) -> bool:
        """
        Disables the data output on the connected IMU.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.configure_data_on_off(cip.EnableCode.Disable)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def enable_trigger(self) -> bool:
        """
        Enables the trigger output on the connected IMU.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.configure_trigger_on_off(cip.EnableCode.Enable)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    def disable_trigger(self) -> bool:
        """
        Disables the trigger output on the connected IMU.

        Returns:
            bool: True on success, False otherwise.
        """
        request_packet = ic.configure_trigger_on_off(cip.EnableCode.Disable)
        reply = self._set(request_packet)
        return bool(reply.sub_packets[0].payload)

    ####################################
    #              Private             #
    ####################################

    def _get(self, request: Callable) -> p.Packet:
        """
        Generic method for getting a value and acknowledging response.

        Args:
            request (function): Function to generate a request packet.

        Returns:
            Result of request.
        """
        request_packet = request()
        packet_type = request_packet.packet_type
        message_code = request_packet.sub_packets[0].message_code

        with concurrent.futures.ThreadPoolExecutor() as executor:
            read = executor.submit(self._read, packet_type, message_code)
            self.conn.write(request_packet.bytes)
            reply = read.result()

        return reply

    def _set(self, request_packet) -> p.Packet:
        """
        Generic method for setting a value and acknowledging response.

        Args:
            request (function): Function to generate a request packet.

        Returns:
            bool: True on success, False otherwise.
        """
        packet_type = request_packet.packet_type
        message_code = request_packet.sub_packets[0].message_code

        with concurrent.futures.ThreadPoolExecutor() as executor:
            read = executor.submit(self._read, packet_type, message_code)
            self.conn.write(request_packet.bytes)
            reply = read.result()

        return reply

    def _read(self, packet_type: cip.MsgType, message_code: Any) -> Any:
        """
        Reads from the IMU until a specific type is received.

        Args:
            packet_type: Type of packet to wait for.
            message_code: Message code to wait for.

        Returns:
            Packet: First packet matching argument criteria.
        """
        timeout = time.time() + 2
        while time.time() < timeout:
            packets = self.conn.read()
            if packets:
                for packet in packets:
                    if packet.packet_type.value == packet_type.value:
                        if (
                            packet.sub_packets[0].message_echo.value
                            == message_code.value
                        ):
                            return packet

        return None
