"""
Message codes.
"""

from enum import Enum, IntEnum
from typing import List


class MsgType(Enum):
    """
    Message type codes.
    """

    Base = 0x01
    Imu = 0x02
    ImuOutput = 0xA2


class MsgCode(Enum):
    """
    Base class for codes to define the type of message.
    """

    pass


class ImuMsgCode(MsgCode):
    """
    Message codes related to IMU measurement configuration.
    """

    BaudRate = 0x01
    Filter = 0x03
    Decimation = 0x04
    SelectSensorsOld = 0x05
    GetInternalSampleRate = 0x06
    ConfigAccelRange = 0x07
    ConfigGyroRange = 0x08
    ConfigAll = 0x09
    DataOnOff = 0x0A
    TriggerOnOff = 0x0B
    SelectSensorsRevB = 0x0C
    ConfigAuxAccelRange = 0x0D
    SetGyros = 0xCC

    Ack = 0x80

    BaudRateReply = 0x81
    # Getting 0x82 reply from SelectSensorsOld
    FilterReply = 0x83
    DecimationReply = 0x84
    SelectSensorsOldReply = 0x85
    GetInternalSampleRateReply = 0x86
    ConfigAccelRangeReply = 0x87
    ConfigGyroRangeReply = 0x88
    ConfigAllReply = 0x89
    DataOnOffReply = 0x8A
    TriggerOnOffReply = 0x8B
    SelectSensorsRevBReply = 0x8C
    ConfigAuxAccelRangeReply = 0x8D


class BaseMsgCode(MsgCode):
    """
    Message codes related to device configuration.
    """

    GetBit = 0x01
    Ping = 0x02
    GetCmds = 0x03
    Reset = 0x04
    GetModel = 0x05
    GetSn = 0x06
    GetFw = 0x07
    GetCal = 0x08
    SetTime = 0x09

    Ack = 0x80

    GetBitReply = 0x81
    PingReply = 0x82
    GetCmdsReply = 0x83
    ResetReply = 0x84
    GetModelReply = 0x85
    GetSnReply = 0x86
    GetFwReply = 0x87
    GetCalReply = 0x88
    SetTimeReply = 0x89


class ImuOutputCode(MsgCode):
    """
    Message codes related to IMU measurements.
    """

    Accel = 0x81
    Gyro = 0x82
    Mag = 0x83
    DeltaTheta = 0x84
    DeltaVelocity = 0x85
    Pressure = 0x86
    Temperature = 0x87
    Time = 0x88
    Internal = 0xAF
    MS3011Raw = 0xB4


class AckCode(Enum):
    """
    Message status codes.
    """

    OK = 0x00
    ErrChecksum = 0x01
    ErrUnknownStructure = 0x02
    ErrUnknownField = 0x03
    ErrInvalidParameter = 0x04


class ValueCode(IntEnum):
    """
    Base class for codes to set a specific value.
    """

    pass


class FunctionCode(ValueCode):
    """
    Message codes for how packet should be used.
    """

    Use = 0x01
    Request = 0x02
    Save = 0x03
    Load = 0x04
    Reset = 0x05


class EnableCode(ValueCode):
    Disable = 0x00
    Enable = 0x01


class FilterCode(ValueCode):
    """
    Codes for filter values.
    """

    Disable = 0x00
    F25 = 0x01
    F50 = 0x02
    F75 = 0x03
    F100 = 0x04
    F10 = 0x05
    F200 = 0x06


class AccelCode(ValueCode):
    """
    Codes for accel values.
    """

    A2 = 0x00
    A4 = 0x01
    A8 = 0x02
    A10 = 0x03
    A15 = 0x04
    A20 = 0x05
    A40 = 0x06


class GyroCode(ValueCode):
    """
    Codes for gyro values.
    """

    G120 = 0x01
    G240 = 0x02
    G480 = 0x03
    G960 = 0x04
    G1920 = 0x05
    G75 = 0x06
    G200 = 0x07


class BaudRateCode(ValueCode):
    """
    Codes for BaudRate values.
    """

    B9600 = 0x00002580
    B19200 = 0x00004B00
    B115200 = 0x0001C200
    B230400 = 0x00038400
    B460800 = 0x00070800
    B921600 = 0x000E1000
