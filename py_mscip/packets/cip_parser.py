"""
Handles parsing raw bytes into packet objects.
"""
from typing import List


class Parser:
    """
    Parses raw bytes into usable packet objects.
    """

    def __init__(self) -> None:
        self._reset()

    def _reset(self) -> None:
        self.buffer: List[int] = []
        self.sync_cnt = 0
        self.packet_length = 0
        self.first_time = True
        self.packets: List[List[int]] = []

    def _sample_complete(self) -> None:
        """
        Completes a packet.
        """
        self.packets.append(self.buffer)
        self.buffer = []
        self.packet_length = 0
        self.sync_cnt = 0

    def parse(self, raw_bytes: bytearray) -> List:
        """
        Parses raw bytes into a packet.
        """

        self.packets = []

        for byte in raw_bytes:
            if self.sync_cnt == 2:  # All synched up

                if self.first_time and (byte == 0xA5):
                    pass
                else:
                    self.buffer.append(byte)
                self.first_time = False

                if len(self.buffer) == 3 + 1:  # get packet length
                    self.packet_length = byte + 2 + 4
                elif len(self.buffer) == self.packet_length:
                    self._sample_complete()
            elif byte == 0xA5:
                self.buffer.append(byte)
                self.sync_cnt += 1
            else:
                self._reset()

        return self.packets
