"""
Objects for message packets and subpackets.
"""

import struct
from py_mscip.packets import cip
from typing import List


class SubPacket:
    """
    The submessage of a packet.
    """

    def __init__(self, code):
        self.message_code = code
        self.message_size = len(self.payload)

    @property
    def bytes(self) -> List[int]:
        """
        The submessage of a packet.
        """

        ret = [self.message_code.value, self.message_size]
        ret.extend(self.payload)
        return ret

    def __len__(self) -> int:
        return len(self.bytes)

    def __repr__(self) -> str:
        return "%s(0x%x)" % (self.__class__.__name__, self.message_code)


class Packet:
    """
    A message packet.
    """

    def __init__(self, packet_type: cip.MsgType):
        self.sub_packets = []
        self.packet_type = packet_type

    def __len__(self) -> int:
        return len(self.header) + self.payload_length + len(self.checksum)

    def __str__(self) -> str:
        return " ".join([str(sub_packet) for sub_packet in self.sub_packets])

    @property
    def payload_length(self) -> int:
        """
        Returns the length of the payload.
        """

        return len(self.payload)

    @property
    def payload(self) -> List[int]:
        """
        Returns a list of the subpackets bytes.
        """

        return [item for sub_packet in self.sub_packets for item in sub_packet.bytes]

    @property
    def header(self) -> List[int]:
        """
        Returns the packet's header information.
        """

        return [0xA5, 0xA5] + [self.packet_type.value, self.payload_length]

    @property
    def checksum(self) -> List[int]:
        """
        Returns the computed checksum of the packet.
        """

        fletcher = compute_fletcher(self.header + self.payload)
        return [fletcher >> 8, fletcher & 0xFF]

    @property
    def bytes(self) -> int:
        """
        Returns the packet as bytes.
        """

        return self.header + self.payload + self.checksum

    def add_subpacket(self, sub_packet: SubPacket) -> None:
        """
        Adds a subpacket to the packet.
        """

        self.sub_packets.append(sub_packet)

    @classmethod
    def from_hex_string(cls, hex_string: str):
        """
        Creates a packet from a hex string.
        """

        return Packet()


class Ack(SubPacket):
    """
    A subpacket for message types acknowledging a base message was received.
    """

    def __init__(
        self, message_echo: cip.MsgCode, error_code: cip.AckCode = cip.AckCode.OK
    ):
        self.payload = [message_echo.value, error_code.value]
        self.error_code = error_code
        self.message_echo = message_echo
        super().__init__(cip.BaseMsgCode.Ack)

    def __repr__(self) -> str:
        return "%s(0x%02x, 0x%02x)" % (
            self.__class__.__name__,
            self.message_code,
            self.error_code,
        )

    def __str__(self) -> str:
        return f"[Ack: {self.message_echo.name} {self.error_code.name}]"

    @classmethod
    def from_bytes(cls, data: bytearray) -> SubPacket:
        """
        Creates an Ack packet from bytes.
        """

        message_echo = data[2]
        error_code = data[3]
        return cls(cip.BaseMsgCode(message_echo), cip.AckCode(error_code))


class AckImu(SubPacket):
    """
    A subpacket for message types acknowledging an IMU message was received.
    """

    def __init__(
        self, message_echo: cip.BaseMsgCode, error_code: cip.AckCode = cip.AckCode.OK
    ):
        self.payload = [message_echo.value, error_code.value]
        self.error_code = error_code
        self.message_echo = message_echo
        super().__init__(cip.ImuMsgCode.Ack)

    def __repr__(self) -> str:
        return "%s(0x%02x, 0x%02x)" % (
            self.__class__.__name__,
            self.message_code,
            self.error_code,
        )

    def __str__(self) -> str:
        return f"[Ack: {self.message_echo.name} {self.error_code.name}]"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates an AckImu packet from bytes.
        """

        message_echo = data[2]
        error_code = data[3]
        return cls(cip.ImuMsgCode(message_echo), cip.AckCode(error_code))


class NoPayload(SubPacket):
    """
    A subpacket for base message types with no payload.
    """

    def __init__(self, message_code: cip.MsgCode):
        self.payload = []
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x)" % (self.__class__.__name__, self.message_code)

    def __str__(self) -> str:
        return f"[{self.message_code.name}]"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a NoPayload packet from bytes.
        """

        message_code = data[0]
        return cls(cip.BaseMsgCode(message_code))


class NoPayloadImu(SubPacket):
    """
    A subpacket for IMU message types with no payload.
    """

    def __init__(self, message_code: cip.BaseMsgCode):
        self.payload = []
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x)" % (self.__class__.__name__, self.message_code)

    def __str__(self) -> str:
        return f"[{self.message_code.name}]"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a NoPayloadImu packet from bytes.
        """

        message_code = data[0]
        return cls(cip.ImuMsgCode(message_code))


class GetString(SubPacket):
    """
    A subpacket for base message types with a string payload.
    """

    def __init__(self, message_code: cip.BaseMsgCode, string: str):
        self.payload = string.rjust(16).encode()
        self.message_code = message_code
        self.string = string
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x, '%s')" % (
            self.__class__.__name__,
            self.message_code,
            self.string,
        )

    def __str__(self) -> str:
        return f"[{self.message_code.name}:  {self.string}]"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a GetString packet from bytes.
        """

        message_code = data[0]
        string = data[2:18].decode().lstrip()
        return cls(cip.BaseMsgCode(message_code), string)


class GetShort(SubPacket):
    """
    A subpacket for base message types with a 2 byte payload.
    """

    def __init__(self, message_code: cip.MsgCode, value: int):
        self.payload = list(value.to_bytes(2, byteorder="big"))
        self.message_code = message_code
        self.value = value
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x, '%d')" % (
            self.__class__.__name__,
            self.message_code,
            self.value,
        )

    def __str__(self) -> str:
        return f"[{self.message_code.name}:  {self.value}]"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a GetShort packet from bytes.
        """

        message_code = data[0]
        value = (data[2] << 8) + data[3]
        return cls(cip.ImuMsgCode(message_code), value)


class ConfigListOfBytes(SubPacket):
    """
    A subpacket for base message configuration types with a list of bytes payload.
    """

    def __init__(
        self,
        message_code: cip.MsgCode,
        function_code: cip.FunctionCode,
        values: List[cip.ImuOutputCode],
    ):
        self.value = values
        value_ints = [x.value for x in values]
        self.payload = [function_code.value] + value_ints
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "TODO"

    def __str__(self) -> str:
        return f"Message code = {self.message_code}"


class ReplyListOfBytes(SubPacket):
    """
    A subpacket for base message reply types with a list of bytes payload.
    """

    def __init__(self, message_code: cip.MsgCode, values: List[cip.ImuOutputCode]):
        self.value = values
        value_ints = [x.value for x in values]
        self.payload = value_ints
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "TODO"

    def __str__(self) -> str:
        return f"Message code = {self.message_code}"


class Request(SubPacket):
    """
    A subpacket for base message request types.
    """

    def __init__(self, message_code: cip.MsgCode, function_code: cip.FunctionCode):
        self.payload = [function_code.value]
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x)" % (self.__class__.__name__, self.message_code)

    def __str__(self) -> str:
        return f"Message code = {self.message_code}"


class ConfigShort(SubPacket):
    """
    A subpacket for base message configuration types with a 2 byte payload.
    """

    def __init__(
        self, message_code: cip.MsgCode, function_code: cip.FunctionCode, value: int
    ):
        self.value = value
        self.payload = [function_code.value] + list(value.to_bytes(2, byteorder="big"))
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x, %d)" % (
            self.__class__.__name__,
            self.message_code,
            self.value,
        )

    def __str__(self) -> str:
        return f"Message code = {self.message_code}"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a ConfigShort packet from bytes.
        """

        message_code = data[0]
        value = int.from_bytes(data[2:3], byteorder="big")
        return cls(cip.BaseMsgCode(message_code), cip.FunctionCode.Request, value)


class ConfigInt(SubPacket):
    """
    A subpacket for base message configuration types with a int payload.
    """

    def __init__(
        self,
        message_code: cip.MsgCode,
        function_code: cip.FunctionCode,
        value: cip.ValueCode,
    ):
        self.value = value
        self.payload = [function_code.value] + list(value.to_bytes(4, byteorder="big"))
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x, %d)" % (
            self.__class__.__name__,
            self.message_code,
            self.value,
        )

    def __str__(self) -> str:
        return f"Message code = {self.message_code}"


class GetCmds(SubPacket):
    """
    A subpacket for base message to get commands.
    """

    def __init__(self, message_code: cip.BaseMsgCode, cmds: List[int]):
        self.cmds = cmds
        self.payload = [b for cmd in cmds for b in cmd.to_bytes(2, byteorder="big")]
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x)" % (self.__class__.__name__, self.message_code)

    def __str__(self) -> str:
        return f'[Cmds: {", ".join([format(cmd, "04x") for cmd in self.cmds])}]'

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a GetCmds packet from bytes.
        """

        message_code = data[0]
        cmds = []
        for i in range(2, len(data), 2):
            cmds.append(int.from_bytes(data[i : i + 2], byteorder="big"))

        return cls(cip.BaseMsgCode(message_code), cmds)


class Floats(SubPacket):
    """
    A subpacket for base message types with a float payload.
    """

    def __init__(self, message_code: cip.BaseMsgCode, values: List[float]):
        self.values = values
        self.payload = [b for value in values for b in struct.pack(">f", value)]
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x)" % (self.__class__.__name__, self.message_code)

    def __str__(self) -> str:
        return " ".join([format(value, "f") for value in self.values])

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a Floats packet from bytes.
        """

        message_code = data[0]
        values = []
        for i in range(2, len(data), 4):
            values.append(struct.unpack(">f", data[i : i + 4])[0])

        return cls(cip.ImuOutputCode(message_code), values)


class SetTime(SubPacket):
    """
    A subpacket for base message to set the time.
    """

    def __init__(self, message_code: cip.BaseMsgCode, week: int, second: int):
        self.payload = list((week.to_bytes(2, byteorder="big"))) + list(
            (second.to_bytes(4, byteorder="big"))
        )
        self.message_code = message_code
        self.week = week
        self.second = second
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x, %d)" % (
            self.__class__.__name__,
            self.message_code,
            self.value,
        )

    def __str__(self) -> str:
        return f"[Set Time: Week = {self.week},  Second = {self.second}]"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a SetTime packet from bytes.
        """

        message_code = data[0]
        week = int.from_bytes(data[2:4], byteorder="big")
        second = int.from_bytes(data[4:8], byteorder="big")

        return cls(cip.BaseMsgCode(message_code), week, second)


class GetTime(SubPacket):
    """
    A subpacket for base message to get the time.
    """

    def __init__(self, message_code: cip.BaseMsgCode, second: int, week: int, flags):
        self.payload = (
            list(struct.pack(">d", second))
            + list((week.to_bytes(2, byteorder="big")))
            + list((flags.to_bytes(2, byteorder="big")))
        )
        self.message_code = message_code
        self.week = week
        self.second = second
        self.flags = flags
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x, %d)" % (
            self.__class__.__name__,
            self.message_code,
            self.value,
        )

    def __str__(self) -> str:
        return f"[Get Time: Week = {self.week},  Second = {self.second}, Flags = {self.flags}]"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a GetTime packet from bytes.
        """

        message_code = data[0]
        second = struct.unpack(">d", data[2:10])[0]
        week = int.from_bytes(data[10:12], byteorder="big")
        flags = int.from_bytes(data[12:14], byteorder="big")
        return cls(cip.BaseMsgCode(message_code), second, week, flags)


class Bit(SubPacket):
    """
    A subpacket for base message types with a bit payload.
    """

    def __init__(self, message_code: cip.BaseMsgCode, value: bool):
        self.value = value
        self.payload = list((value.to_bytes(3, byteorder="big")))
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x, %d)" % (
            self.__class__.__name__,
            self.message_code,
            self.value,
        )

    def __str__(self) -> str:
        return f'[BIT: 0x{format(self.value, "06x")}]'

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a Bit packet from bytes.
        """

        message_code = data[0]
        bit = int.from_bytes(data[2:5], byteorder="big")
        return cls(cip.BaseMsgCode(message_code), bit)


class ConfigByte(SubPacket):
    """
    A subpacket for base message configuration types with a byte payload.
    """

    def __init__(
        self,
        message_code: cip.MsgCode,
        function_code: cip.FunctionCode,
        value: cip.ValueCode,
    ):
        self.value = value
        if isinstance(value, int):
            self.payload = [function_code.value, value]
        else:
            self.payload = [function_code.value, value.value]
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x, %d)" % (
            self.__class__.__name__,
            self.message_code,
            self.value,
        )

    def __str__(self) -> str:
        return f"Message code = {self.message_code}"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a ConfigByte packet from bytes.
        """

        message_code = data[0]
        value = int.from_bytes(data[2:5], byteorder="big")
        return cls(cip.BaseMsgCode(message_code), value)


class SetByte(SubPacket):
    """
    A subpacket for base message to set the time.
    """

    def __init__(self, message_code: cip.BaseMsgCode, value: int):
        self.value = value
        if isinstance(value, int):
            self.payload = [value]
        else:
            self.payload = [value.value]
        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x, %d)" % (
            self.__class__.__name__,
            self.message_code,
            self.value,
        )

    def __str__(self) -> str:
        return f"Message code = {self.message_code}"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a SetByte packet from bytes.
        """

        message_code = data[0]
        value = int.from_bytes(data[2:5], byteorder="big")
        return cls(cip.BaseMsgCode(message_code), value)


class SetListOfBytes(SubPacket):
    """
    A subpacket for base message to set a list of bytes.
    """

    def __init__(
        self,
        message_code: cip.ImuMsgCode,
        function_code: cip.FunctionCode,
        value: bool,
    ):
        self.value = value
        self.payload = [function_code.value] + value

        self.message_code = message_code
        super().__init__(message_code)

    def __repr__(self) -> str:
        return "%s(0x%02x, %d)" % (
            self.__class__.__name__,
            self.message_code,
            self.value,
        )

    def __str__(self) -> str:
        return f"Message code = {self.message_code}"

    @classmethod
    def from_bytes(cls, data: bytearray):
        """
        Creates a SetListOfBytes packet from bytes.
        """

        message_code = data[0]
        value = int.from_bytes(data[2:5], byteorder="big")
        return cls(cip.BaseMsgCode(message_code), value)


def format_hex(raw_data: List[int]) -> str:
    """
    Formats raw data into hex values.

    Args:
        raw_data (str): Raw message to be formatted.

    Returns:
        float: Converted hex value.
    """
    return " ".join(format(p, "02x") for p in raw_data)


def format_floats(raw_data: List[int]) -> str:
    """
    Formats data into float values.

    Args:
        raw_data (str): Raw message to be formatted.

    Returns:
        float: Converted float value.
    """
    return " ".join(format(p, "f") for p in raw_data)


def verify_checksum(raw_data: List[int]) -> bool:
    """
    Compute and verify the checksum of a packet.

    Args:
        raw_data (str): Raw message to be verified.

    Returns:
        bool: True if a valid data, False otherwise.
    """
    computed_fletcher = compute_fletcher(raw_data[:-2])
    raw_data_fletcher = (raw_data[-2] << 8) + raw_data[-1]
    return computed_fletcher == raw_data_fletcher


def compute_fletcher(packet: List[int]) -> int:
    """
    Computes the fletcher checksum for a packet.

    Args:
        packet (Packet): The packet to compute the checksum for.

    Returns:
        int: The computed checksum.
    """

    checksum_1 = 0
    checksum_2 = 0

    for byte in packet:
        checksum_1 += byte
        checksum_2 += checksum_1
        checksum_1 &= 0xFF
        checksum_2 &= 0xFF

    fletch = (checksum_1 << 8) + checksum_2
    return fletch & 0xFFFF
