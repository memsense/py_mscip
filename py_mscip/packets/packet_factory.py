"""
Convenience methods for creating packets and subpackets.
"""

from py_mscip.packets.packet import (
    Packet,
    Ack,
    NoPayload,
    GetString,
    SetTime,
    GetCmds,
    Bit,
    Floats,
    GetTime,
    GetShort,
    NoPayloadImu,
    AckImu,
)
from py_mscip.packets.cip import BaseMsgCode, ImuMsgCode, MsgType, ImuOutputCode

IMU_LOOKUP = {
    ImuMsgCode.Decimation: GetShort,
    ImuMsgCode.Ack: AckImu,
    ImuMsgCode.GetInternalSampleRate: NoPayloadImu,
    ImuMsgCode.GetInternalSampleRateReply: GetShort,
    ImuMsgCode.SelectSensorsRevB: NoPayloadImu,
    ImuMsgCode.SelectSensorsRevBReply: GetShort,
}

IMU_OUTPUT_LOOKUP = {
    ImuOutputCode.Accel: Floats,
    ImuOutputCode.Gyro: Floats,
    ImuOutputCode.Mag: Floats,
    ImuOutputCode.Pressure: Floats,
    ImuOutputCode.Temperature: Floats,
    ImuOutputCode.DeltaTheta: Floats,
    ImuOutputCode.DeltaVelocity: Floats,
    ImuOutputCode.Time: GetTime,
    ImuOutputCode.Internal: Floats,
    ImuOutputCode.MS3011Raw: Floats,
}


BASE_LOOKUP = {
    BaseMsgCode.GetBit: NoPayload,
    BaseMsgCode.Ping: NoPayload,
    BaseMsgCode.GetCmds: NoPayload,
    BaseMsgCode.Reset: NoPayload,
    BaseMsgCode.GetModel: NoPayload,
    BaseMsgCode.GetSn: NoPayload,
    BaseMsgCode.GetFw: NoPayload,
    BaseMsgCode.GetCal: NoPayload,
    BaseMsgCode.SetTime: SetTime,
    BaseMsgCode.Ack: Ack,
    BaseMsgCode.GetBitReply: Bit,
    BaseMsgCode.GetCmdsReply: GetCmds,
    BaseMsgCode.GetModelReply: GetString,
    BaseMsgCode.GetSnReply: GetString,
    BaseMsgCode.GetFwReply: GetString,
    BaseMsgCode.GetCalReply: GetString,
}


def create_packet(data):
    """
    Creates a packet from a byte array.
    """

    payload_size = data[3]
    msg_type = MsgType(data[2])
    packet = Packet(msg_type)
    sub_start = 4

    while payload_size > 0:
        sub_size = data[sub_start + 1] + 2
        packet.add_subpacket(
            create_sub_packet(msg_type, data[sub_start : sub_start + sub_size])
        )
        payload_size -= sub_size
        sub_start += sub_size
    return packet


def create_packet_from_hex(hex_data):
    """
    Creates a packet from a hex string.
    """

    return create_packet(bytearray.fromhex(hex_data))


def create_sub_packet(msg_type, data):
    """
    Creates a sub packet from a byte array.
    """

    if msg_type == MsgType.Base:
        code = BaseMsgCode(data[0])
        return BASE_LOOKUP[code].from_bytes(data)
    if msg_type == MsgType.Imu:
        code = ImuMsgCode(data[0])
        return IMU_LOOKUP[code].from_bytes(data)
    if msg_type == MsgType.ImuOutput:
        code = ImuOutputCode(data[0])
        return IMU_OUTPUT_LOOKUP[code].from_bytes(data)

    raise ValueError(f"Unknown message type: {msg_type}")


def create_sub_packet_from_hex(msg_type, hex_data):
    """
    Creates a sub packet from a hex string.
    """

    return create_sub_packet(msg_type, bytearray.fromhex(hex_data))
