from py_mscip.interfaces.config import Config
import pytest  # type: ignore
import json
import os


def test_init():
    config = Config()
    assert config._config.get("sn") == None
    assert config._config.get("model") == None
    assert config._config.get("fw") == None


def test_load():
    test_file = "tests\\test_load.json"
    try:
        test_config = {"sn": "25187", "model": "MS-IMU3030M", "fw": "R_1_1_2_B6"}
        with open(test_file, "w") as file:
            json.dump(test_config, file, indent=4)

        config = Config()
        config.load(test_file)

        assert config._config["sn"] == "25187"
        assert config._config["model"] == "MS-IMU3030M"
        assert config._config["fw"] == "R_1_1_2_B6"

    finally:
        if os.path.exists(test_file):
            os.remove(test_file)


def test_save():
    test_file = "tests\\test_save.json"
    try:
        config = Config(sn="25187", model="MS-IMU3030M", fw="R_1_1_2_B6")
        config.save(test_file)

        with open(test_file) as file:
            data = json.load(file)

        assert data["sn"] == "25187"
        assert data["model"] == "MS-IMU3030M"
        assert data["fw"] == "R_1_1_2_B6"

    finally:
        if os.path.exists(test_file):
            os.remove(test_file)
