from py_mscip.interfaces.conn import Conn
import py_mscip.packets.packet as p
import pytest  # type: ignore
import time


class mock_serial:
    def __init__(self, comport, baudrate):
        return

    @property
    def in_waiting(self):
        return True

    def read(self, waiting):
        return b"\xa5\xa5\xa2\"\x82\x0c:\xbb\x02\xaf\xbd'\xa0u<<\xe1\x99\x81\x0c<\x01\x0f\xc9:\x1a\x15\xa0?\x7f\xff\x18\x87\x04BB\xff\xf8\xb3\xc7"


@pytest.fixture
def serial_connection():
    # from serial import SerialException, Serial
    # return Serial("COM3", 460800)

    return mock_serial("COM3", 460800)


def test_init(serial_connection):
    conn = Conn(serial_connection)

    assert conn.packet_format == "info"


def test_read(serial_connection):
    conn = Conn(serial_connection)

    timeout = time.time() + 1
    count = 0
    while count < 10 and time.time() < timeout:
        data = conn.read()
        if data:
            count += 1

    assert count == 10


def test_format_hex(serial_connection):
    conn = Conn(serial_connection)

    packet = [
        165,
        165,
        162,
        34,
        129,
        12,
        187,
        56,
        176,
        145,
        57,
        50,
        40,
        109,
        63,
        128,
        45,
        254,
        130,
        12,
        188,
        70,
        9,
        164,
        60,
        48,
        10,
        0,
        189,
        81,
        244,
        110,
        135,
        4,
        66,
        66,
        127,
        248,
        98,
        32,
    ]
    hex = p.format_hex(packet)

    assert (
        hex
        == "a5 a5 a2 22 81 0c bb 38 b0 91 39 32 28 6d 3f 80 2d fe 82 0c bc 46 09 a4 3c 30 0a 00 bd 51 f4 6e 87 04 42 42 7f f8 62 20"
    )


def test_format_floats(serial_connection):
    conn = Conn(serial_connection)

    packet = [
        165,
        165,
        162,
        34,
        129,
        12,
        187,
        56,
        176,
        145,
        57,
        50,
        40,
        109,
        63,
        128,
        45,
        254,
        130,
        12,
        188,
        70,
        9,
        164,
        60,
        48,
        10,
        0,
        189,
        81,
        244,
        110,
        135,
        4,
        66,
        66,
        127,
        248,
        98,
        32,
    ]
    floats = p.format_floats(packet)

    assert (
        floats
        == "165.000000 165.000000 162.000000 34.000000 129.000000 12.000000 187.000000 56.000000 176.000000 145.000000 57.000000 50.000000 40.000000 109.000000 63.000000 128.000000 45.000000 254.000000 130.000000 12.000000 188.000000 70.000000 9.000000 164.000000 60.000000 48.000000 10.000000 0.000000 189.000000 81.000000 244.000000 110.000000 135.000000 4.000000 66.000000 66.000000 127.000000 248.000000 98.000000 32.000000"
    )


def test_verify_checksum(serial_connection):
    conn = Conn(serial_connection)

    valid_packet = [
        165,
        165,
        162,
        34,
        129,
        12,
        187,
        56,
        176,
        145,
        57,
        50,
        40,
        109,
        63,
        128,
        45,
        254,
        130,
        12,
        188,
        70,
        9,
        164,
        60,
        48,
        10,
        0,
        189,
        81,
        244,
        110,
        135,
        4,
        66,
        66,
        127,
        248,
        98,
        32,
    ]
    valid = p.verify_checksum(valid_packet)

    assert valid

    invalid_packet = [
        164,
        165,
        162,
        34,
        129,
        12,
        187,
        56,
        176,
        145,
        57,
        50,
        40,
        109,
        63,
        128,
        45,
        254,
        130,
        12,
        188,
        70,
        9,
        164,
        60,
        48,
        10,
        0,
        189,
        81,
        244,
        110,
        135,
        4,
        66,
        66,
        127,
        248,
        98,
        32,
    ]
    invalid = p.verify_checksum(invalid_packet)

    assert not invalid
