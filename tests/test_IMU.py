import pytest  # type: ignore
from py_mscip.interfaces.conn import Conn
from py_mscip.interfaces.imu import IMU
from py_mscip.packets import cip
from queue import Queue


class mock_serial:
    _queue: Queue = Queue()

    def __init__(self, comport, baudrate):
        return

    def write(self, data):
        self._queue.put(data)

    @property
    def in_waiting(self):
        return self._queue.qsize()

    def read(self, waiting):
        request = self._queue.get()

        # ping
        if request == [165, 165, 1, 2, 2, 0, 79, 37]:
            return b"\xa5\xa5\x01\x04\x80\x02\x02\x00\xd3\xcf"
        # reset
        if request == [165, 165, 1, 2, 4, 0, 81, 41]:
            return b"\xff"
        # get_bit
        if request == [165, 165, 1, 2, 1, 0, 78, 35]:
            return b"\xa5\xa5\x01\x04\x80\x02\x01\x03\xd5\xd0"

        # get_cmds
        # How to interpret the commands?
        if request == [165, 165, 1, 2, 3, 0, 80, 39]:
            return b"\xa5\xa5\x018\x80\x02\x03\x00\x832\x01\x02\x01\x03\x01\x04\x01\x05\x01\x06\x01\x07\x01\x08\x01\t\x02\x01\x02\x03\x02\x04\x02\x05\x02\x06\x02\x08\x02\t\x02\n\x02\x0b\x02\x0c\xa2\x81\xa2\x82\xa2\x84\xa2\x85\xa2\x87\xa2\x88\xa2\x83V\x80"

        # get_model
        if request == [165, 165, 1, 2, 5, 0, 82, 43]:
            return b"\xa5\xa5\x01\x16\x80\x02\x05\x00\x85\x10     MS-IMU3030M\xe81"
        # get_sn
        if request == [165, 165, 1, 2, 6, 0, 83, 45]:
            return b"\xa5\xa5\x01\x16\x80\x02\x06\x00\x86\x10           25187\xe67"
        # get_fw
        if request == [165, 165, 1, 2, 7, 0, 84, 47]:
            return b"\xa5\xa5\x01\x16\x80\x02\x07\x00\x87\x10      R_1_1_2_B6\x1b\xa1"
        # get_cal_date
        if request == [165, 165, 1, 2, 8, 0, 85, 49]:
            return b"\xa5\xa5\x01\x16\x80\x02\x08\x00\x88\x10      02-13-2018.\xe1"
        # get_isr
        if request == [165, 165, 2, 2, 6, 0, 84, 49]:
            return b"\xa5\xa5\x02\x08\x80\x02\x06\x00\x86\x02\x03 \x87\xa5"
        # get_select_sensors
        if request == [165, 165, 2, 3, 12, 1, 2, 94, 159]:
            return b"\xa5\xa5\x02\r\x80\x02\x0c\x00\x8c\x07\x82\x81\x83\x84\x85\x87\x88\x18\xb1"

        # set_baudrate
        if request == [165, 165, 2, 7, 1, 5, 1, 0, 7, 8, 0, 105, 34]:
            return b"\xa5\xa5\x02\x04\x80\x02\x01\x00\xd3\xd3"
        # set_filter
        if request == [165, 165, 2, 4, 3, 2, 1, 1, 87, 224]:
            return b"\xa5\xa5\x02\x04\x80\x02\x03\x00\xd5\xd7"
        # set_accel
        if request == [165, 165, 2, 4, 7, 2, 1, 0, 90, 239]:
            return b"\xa5\xa5\x02\x04\x80\x02\x07\x00\xd9\xdf"
        # set_gyro
        if request == [165, 165, 2, 4, 8, 2, 1, 7, 98, 250]:
            return b"\xa5\xa5\x02\x04\x80\x02\x08\x00\xda\xe1"
        # set_decimation
        if request == [165, 165, 2, 5, 4, 3, 1, 0, 8, 97, 76]:
            return b"\xa5\xa5\x02\x04\x80\x02\x04\x00\xd6\xd9"
        # set_select_sensors
        if request == [165, 165, 2, 4, 12, 2, 1, 129, 224, 132]:
            return b"\xa5\xa5\x02\x04\x80\x02\x0c\x00\xde\xe9"

        # save_startup_settings
        if request == [165, 165, 2, 3, 9, 1, 3, 92, 151]:
            return b"\xa5\xa5\x02\x04\x80\x02\t\x00\xdb\xe3"
        # load_startup_settings
        if request == [165, 165, 2, 3, 9, 1, 4, 93, 152]:
            return b"\xa5\xa5\x02\x04\x80\x02\t\x00\xdb\xe3"
        # reset_startup_settings
        if request == [165, 165, 2, 3, 9, 1, 5, 94, 153]:
            return b"\xa5\xa5\x02\x04\x80\x02\t\x00\xdb\xe3"

        # enable_data
        if request == [165, 165, 2, 4, 10, 2, 1, 1, 94, 252]:
            return b"\xa5\xa5\x02\x04\x80\x02\n\x00\xdc\xe5"
        # disable_data
        if request == [165, 165, 2, 4, 10, 2, 1, 0, 93, 251]:
            return b"\xa5\xa5\x02\x04\x80\x02\n\x00\xdc\xe5"
        # enable_trigger
        if request == [165, 165, 2, 4, 11, 2, 1, 1, 95, 0]:
            return b"\xa5\xa5\x02\x04\x80\x02\x0b\x00\xdd\xe7"
        # disable_trigger
        if request == [165, 165, 2, 4, 11, 2, 1, 0, 94, 255]:
            return b"\xa5\xa5\x02\x04\x80\x02\x0b\x00\xdd\xe7"

        return "err"


@pytest.fixture
def imu():
    # from serial import Serial

    # ser = Serial("COM3", 460800)

    ser = mock_serial("COM3", 460800)

    return IMU(ser)


def test_ping(imu):
    result = imu.ping()
    assert result


def test_reset(imu):
    # No reply received
    result = imu.reset()
    assert result == None


def test_get_bit(imu):
    result = imu.get_bit()
    assert result == [1, 3]


def test_get_cmds(imu):
    # How to interpret commands
    result = imu.get_cmds()
    assert result == [
        1,
        2,
        1,
        3,
        1,
        4,
        1,
        5,
        1,
        6,
        1,
        7,
        1,
        8,
        1,
        9,
        2,
        1,
        2,
        3,
        2,
        4,
        2,
        5,
        2,
        6,
        2,
        8,
        2,
        9,
        2,
        10,
        2,
        11,
        2,
        12,
        162,
        129,
        162,
        130,
        162,
        132,
        162,
        133,
        162,
        135,
        162,
        136,
        162,
        131,
    ]


def test_get_model(imu):
    result = imu.get_model()
    assert result == "MS-IMU3030M"


def test_get_sn(imu):
    result = imu.get_sn()
    assert result == "25187"


def test_get_fw(imu):
    result = imu.get_fw()
    assert result == "R_1_1_2_B6"


def test_get_cal_date(imu):
    result = imu.get_cal_date()
    assert result == "02-13-2018"


def test_get_isr(imu):
    result = imu.get_isr()
    assert result == 800


def test_get_select_sensors(imu):
    result = imu.get_select_sensors()
    print(result)
    assert result == [cip.ImuOutputCode.Gyro, cip.ImuOutputCode.Accel]


def test_set_baudrate(imu):
    result = imu.set_baudrate(460800)
    assert result


def test_set_filter(imu):
    result = imu.set_filter(cip.FilterCode.F25)
    assert result


def test_set_accel(imu):
    result = imu.set_accel(cip.AccelCode.A2)
    assert result


def test_set_gyro(imu):
    result = imu.set_gyro(cip.GyroCode.G200)
    assert result


def test_set_decimation(imu):
    result = imu.set_decimation(8)
    assert result


def test_set_select_sensors(imu):
    result = imu.set_select_sensors([cip.ImuOutputCode.Accel])
    assert result


def test_save_startup_settings(imu):
    result = imu.save_startup_settings()
    assert result


def test_load_startup_settings(imu):
    result = imu.load_startup_settings()
    assert result


def test_reset_startup_settings(imu):
    result = imu.reset_startup_settings()
    assert result


def test_enable_data(imu):
    result = imu.enable_data()
    assert result


def test_disable_data(imu):
    result = imu.disable_data()
    assert result


def test_enable_trigger(imu):
    result = imu.enable_trigger()
    assert result


def test_disable_trigger(imu):
    result = imu.disable_trigger()
    assert result
