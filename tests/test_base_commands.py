from py_mscip.commands import base_commands as bc
from py_mscip.packets import packet_factory as pf
import pytest  # type: ignore


def test_ping():
    expected = list(bytearray.fromhex("A5A5010202004F25"))
    assert expected == bc.ping().bytes


def test_ping_reply():
    expected = list(bytearray.fromhex("A5A5010480020200D3CF"))
    assert expected == bc.ping_reply().bytes


def test_reset():
    expected = list(bytearray.fromhex("A5A5010204005129"))
    assert expected == bc.reset().bytes


def test_reset_reply():
    expected = list(bytearray.fromhex("A5A5010480020400D5D3"))
    assert expected == bc.reset_reply().bytes


def test_get_bit():
    expected = list(bytearray.fromhex("A5A5010201004E23"))
    assert expected == bc.get_bit().bytes


def test_get_bit_reply():
    bit = False
    expected = list(bytearray.fromhex("A5A501098002010081030000005BAA"))
    assert expected == bc.get_bit_reply(bit).bytes


def test_get_cmds():
    expected = list(bytearray.fromhex("A5A5010203005027"))
    assert expected == bc.get_cmds().bytes


def test_get_cmds_reply():
    cmds = [0x102, 0x103, 0x104, 0x105]
    expected = list(bytearray.fromhex("A5A5010E80020300830801020103010401057B57"))
    assert expected == bc.get_cmds_reply(cmds).bytes


def test_get_model():
    expected = list(bytearray.fromhex("A5A501020500522B"))
    assert expected == bc.get_model().bytes


def test_get_model_reply():
    model_name = "MS_IMU3020"
    expected = list(
        bytearray.fromhex("A5A501168002050085102020202020204D535F494D5533303230EC54")
    )
    assert expected == bc.get_model_reply(model_name).bytes


def test_get_sn():
    expected = list(bytearray.fromhex("A5A501020600532D"))
    assert expected == bc.get_sn().bytes


def test_get_sn_reply():
    serial_number = "20268"
    expected = list(
        bytearray.fromhex("A5A5011680020600861020202020202020202020203230323638E123")
    )
    assert expected == bc.get_sn_reply(serial_number).bytes


def test_get_fw():
    expected = list(bytearray.fromhex("A5A501020700542F"))
    assert expected == bc.get_fw().bytes


def test_get_fw_reply():
    firmware_version = "R_1_2_3"
    expected = list(
        bytearray.fromhex("A5A50116800207008710202020202020202020525F315F325F33A625")
    )
    assert expected == bc.get_fw_reply(firmware_version).bytes


def test_get_cal():
    expected = list(bytearray.fromhex("A5A5010208005531"))
    assert expected == bc.get_cal().bytes


def test_get_cal_reply():
    cal_date = "05-08-2015"
    expected = list(
        bytearray.fromhex("A5A5011680020800881020202020202030352D30382D323031353210")
    )
    assert expected == bc.get_cal_reply(cal_date).bytes


def test_set_time():
    week = 1839
    second = 767
    expected = list(bytearray.fromhex("A5A501080906072F000002FF99AF"))
    assert expected == bc.set_time(week, second).bytes


def test_set_time_reply():
    expected = list(bytearray.fromhex("A5A5010480020900DADD"))
    assert expected == bc.set_time_reply().bytes


@pytest.mark.parametrize(
    "msg",
    [
        "A5A5010201004E23",
        "A5A501098002010081030000005BAA",
        "A5A5010202004F25",
        "A5A5010480020200D3CF",
        "A5A5010203005027",
        "A5A5010E80020300830801020103010401057B57",
        "A5A5010204005129",
        "A5A5010480020400D5D3",
        "A5A501020500522B",
        "A5A501168002050085102020202020204D535F494D5533303230EC54",
        "A5A501020600532D",
        "A5A5011680020600861020202020202020202020203230323638E123",
        "A5A501020700542F",
        "A5A50116800207008710202020202020202020525F315F325F33A625",
        "A5A5010208005531",
        "A5A5011680020800881020202020202030352D30382D323031353210",
        "A5A501080906072F000002FF99AF",
        "A5A5010480020900DADD",
    ],
)
def test_test(msg):

    assert pf.create_packet_from_hex(msg).bytes == list(bytearray.fromhex(msg))
    pass
