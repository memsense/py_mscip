from py_mscip.packets.cip_parser import Parser


def test_empty():
    parser = Parser()
    actual = parser.parse(bytearray())
    assert [] == actual


def test_ping():
    parser = Parser()
    ping_msg = bytearray.fromhex("A5A5010201004E23")
    actual = parser.parse(ping_msg)
    assert list(ping_msg) == actual[0]


def test_ping_padded():
    parser = Parser()
    msg = "A5A5010201004E23"
    msg_padded = "FF01" + msg + "ABCD"
    ping_msg = bytearray.fromhex(msg)
    ping_msg_padded = bytearray.fromhex(msg_padded)
    actual = parser.parse(ping_msg)
    assert list(ping_msg) == actual[0]
