from py_mscip.commands import imu_commands as ic
from py_mscip.packets import cip
import pytest  # type: ignore


def test_get_isr():
    expected = list(bytearray.fromhex("A5A5020206005431"))
    assert expected == ic.get_isr().bytes


def test_get_isr_reply():
    expected = list(bytearray.fromhex("A5A50208800206008602032087A5"))
    assert expected == ic.get_isr_reply(800).bytes


def test_configure_filter():
    expected = list(bytearray.fromhex("A5A502040302010258E1"))
    assert expected == ic.configure_filter(cip.FilterCode.F50).bytes


def test_configure_filter_reply():
    expected = list(bytearray.fromhex("A5A5020480020300D5D7"))
    assert expected == ic.configure_filter_reply().bytes


def test_configure_decimation():
    expected = list(bytearray.fromhex("A5A5020504030100126B56"))
    assert expected == ic.configure_decimation(0x12).bytes


def test_configure_accel():

    expected = list(bytearray.fromhex("A5A50204070201025CF1"))
    assert expected == ic.configure_accel(cip.AccelCode.A8).bytes


def test_configure_gyro():
    expected = list(bytearray.fromhex("A5A50204080201025DF5"))
    assert expected == ic.configure_gyro(cip.GyroCode.G240).bytes


def test_configure_data_on_off():
    expected = list(bytearray.fromhex("A5A502040A0201015EFC"))
    assert expected == ic.configure_data_on_off(cip.EnableCode.Enable).bytes


def test_configure_trigger_on_off():
    expected = list(bytearray.fromhex("A5A502040B0201015F00"))
    assert expected == ic.configure_trigger_on_off(cip.EnableCode.Enable).bytes


def test_configure_baudrate():
    expected = list(bytearray.fromhex("A5A502070105010001C2001D84"))
    assert expected == ic.configure_baudrate(115200).bytes


def test_configure_select_sensors():
    expected = list(bytearray.fromhex("A5A502050C0301818264F0"))
    sensors = [cip.ImuOutputCode.Accel, cip.ImuOutputCode.Gyro]
    assert expected == ic.configure_select_sensors(sensors).bytes


def test_configure_select_sensors_reply():
    expected = list(bytearray.fromhex("A5A5020480020C00DEE9"))
    assert expected == ic.configure_select_sensors_reply().bytes


def test_get_select_sensors():
    expected = list(bytearray.fromhex("A5 A5 02 03 0C 01 02 5E 9F"))
    assert expected == ic.get_select_sensors().bytes


def test_get_select_sensors_reply():
    expected = list(bytearray.fromhex("A5 A5 02 08 80 02 0C 00 8C 02 81 82 73 3F"))
    sensors = [cip.ImuOutputCode.Accel, cip.ImuOutputCode.Gyro]
    assert expected == ic.get_select_sensors_reply(sensors).bytes


def test_config_all():
    expected = list(bytearray.fromhex("A5A502030901035C97"))
    assert expected == ic.config_all(cip.FunctionCode.Save).bytes


def test_config_all_reply():
    expected = list(bytearray.fromhex("A5A5020480020900DBE3"))
    assert expected == ic.config_all_reply().bytes
