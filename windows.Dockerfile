# Change docker-desktop to "Switch to windows containers"
FROM python:3.8.2-windowsservercore-1809

RUN mkdir c:\py_mscip
WORKDIR c:\\py_mscip
COPY requirements.txt .

RUN python -m pip install -r requirements.txt